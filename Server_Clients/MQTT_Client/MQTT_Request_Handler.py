#####################################################################
######################## IMPORTS ####################################
#####################################################################

import logging
import pickle

from MQTT_Client_Function_Table import request_handler_table
import SocketServer


#####################################################################
######################## CONSTANTS ##################################
#####################################################################
SUCCESS = 0

#####################################################################
######################## CODE #######################################
#####################################################################


## request_handler
#
#   This is the base request handle to the server.
#   All handles are individual threaded processes.
#
class mqtt_request_handler(SocketServer.BaseRequestHandler):

    # The logger
    logger              = None

    # The mqtt client connection
    mqtt_handle         = None

    def set_handle(self, mqtt_handle):
        self.mqtt_handle = mqtt_handle
        return

    ## handle
    #
    #   This is the overridden method form the socketServer API.
    #
    def handle(self):

        # Set a logger
        self.set_logger()

        # Set an mqtt handle
        self.mqtt_handle = self.server.mqtt_handle

        # self.request is the client connection
        data = pickle.loads(self.request.recv(1024))  # clip input at 1Kb

        self.logger.info('Received data %s' %data)

        # The commands are as follows:
        #   - COMMAND : TOPIC

        # We split the command from the args
        data = data.split(':')

        # We check validity
        if not request_handler_table.has_key(data[0]):

            # Not a good request - this will timeout the connection
            request_handler_table['ERROR'](self, data)
            return

        # Check the sensor channel validity
        if not (('control/' in data[1].lower()) and ('_cmd' in data[1].lower())):
            # Not a good request - this will timeout the connection
            request_handler_table['ERROR'](self, data)
            return

        # We then call the appropriate method to execute.
        # We get the request handler and pass the args
        #   - request_handler - args

        self.logger.info('Executing request command: %s' %(data[0]))

        if not self.server.debug:
            status = request_handler_table[data[0]](self, data[1])

            self.logger.info('Execution status: %d' %status)
            # We return the status of the operation
            self.request.send(pickle.loads(str(status)))
        else :
            self.request.sendall(pickle.dumps("response"))
        return

    ## set_logger
    #
    #   This sets the logger instance
    #
    def set_logger(self):

        self.logger = logging.getLogger('mqtt_request_handler')
        return
