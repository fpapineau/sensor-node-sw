__author__ = 'francispapineau'

#####################################################################
######################## IMPORTS ####################################
#####################################################################

import sys

from Plugin_Engine.PluginTypes.Client_Plugin import client_plugin
from Server_Clients.Emoncms_Client.Emoncms_Client import emoncms_server_interface

#####################################################################
######################## CONSTANTS ##################################
#####################################################################

# N/A

#####################################################################
######################## CODE #######################################
#####################################################################
## calculation_plugin
#
#   This is a generic calculation plugin class that is used for any
#   app that needs to calculate data onto the command line interface.
#
class emoncms_client_plugin(client_plugin, object):
    
    # Plugin file name
    plugin_file_name    = 'Emoncms_Client_Plugin.conf'

    # Client handle
    handle              = None
    
    ## __init__
    #
    # The default Constructor
    #
    def __init__(self):
        self.auto_start = True
        client_plugin.__init__(self)
        return

    ## setup
    #
    #    The worker method.
    #
    def setup(self):

        self.set_config_location(self.plugin_file_name)

        configs = self.read_configs()
        if configs is not None:
            string = "Booting EMONCMS client engine ".ljust(75, '.')
            try:

                # Loading the driver
                self.handle = emoncms_server_interface(
                    configs.get("Emoncms_Application_Configuration", "Protocol"),
                    configs.get("Emoncms_Application_Configuration", "Domain"),
                    configs.get("Emoncms_Application_Configuration", "Path"),
                    configs.get("Emoncms_Application_Configuration", "Api")
                )
            except Exception:
                string += ' [ Failed ]'
                self.logger_object.error(string)
                self.logger_object.info("Exiting main context.")
                sys.exit(1)
            return
        else:
            self.logger_object.error("No configs loaded.")
        return

    # ------- Thread Level --------

    ## start_server
    #
    #   starts the client if connected
    #
    def run(self):
        self.logger_object.info("Starting the plugin.")
        try:
            self.connect()
            # Start the engine
            self.handle.start()
        except Exception:
            self.logger_object.error("Failed to start")
        return
    
    ## stop
    #
    #   shuts the client down
    #
    def stop(self):
        self.logger_object.info("Stopping the plugin.")
        self.handle.kill()
        return

    # ------- Connection Level --------

    ## disconnect
    #
    #   disconnects the client and shuts it down
    #
    def disconnect(self):
        self.logger_object.info("Disconnecting the plugin.")
        self.handle.disconnect_ipc()
        return

    ## connect
    #
    #   Connects the client and starts it up
    #
    def connect(self):
        self.logger_object.info("Connecting the plugin.")
        self.handle.connect_ipc()
        return
