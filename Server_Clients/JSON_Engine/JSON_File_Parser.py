

#####################################################################
######################## IMPORTS ####################################
#####################################################################

import json
import logging
import os

import simplejson


#####################################################################
######################## CONSTANTS ##################################
#####################################################################
# N/A
#####################################################################
######################## CODE #######################################
#####################################################################
## json_file_parser
#
#   This is the class in which the JSON dictionary is parsed and where this
#   dict is passed to the emoncms engine.
#
class json_file_parser:

    # The file path to watch
    file_path                   = None

    # The parsed data
    data                        = None

    # The json data
    json_data                   = None

    # The logger
    logger                      = None

    ## __init__
    #
    #   This is the default constructor for the class.
    #
    #   @param path                 - the file path to parse
    #
    def __init__(self, path):

        # The file path to open and process
        self.file_path = path

        self.logger = logging.getLogger("json_file_parser")
        self.logger.info("Opening the file")

        # We call the open method
        self.open_file()
        return

    ## open_file
    #
    #   This method opens a file for processing
    #
    def open_file(self):

        # Se if the file is valid
        try:

            # We open the file
            self.data = open(self.file_path)
            self.logger.info("Successfully opened the json file [%s]" %self.file_path)

        except OSError:
            # handle error here
            self.logger.error("Could not open [%s]" %self.file_path)
            return
        return

    ## process_file
    #
    #   This loads and parses the json file
    #
    def process_file(self):
        self.json_data = simplejson.loads(self.data.read())
        self.logger.info("Successfully loaded the JSON data")
        return self.json_data
