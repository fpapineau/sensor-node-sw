__author__ = 'francispapineau'

#####################################################################
######################## IMPORTS ####################################
#####################################################################

import simplejson
import logging
import threading
import SocketServer

from UDP_Server_IPC import udp_server_ipc

#####################################################################
######################## CONSTANTS ##################################
#####################################################################

ADDRESS             = ""
PORT                = 9996

# Commands
START_REMOTE        = "START_REMOTE"
STOP_REMOTE         = "STOP_REMOTE"

#####################################################################
######################## CODE #######################################
#####################################################################

## ThreadedUDPRequestServer
#
#   This overrides the base UDP Server class and allows for threading to
#   be allowed.
#
class threaded_udp_request_server(SocketServer.ThreadingMixIn, SocketServer.UDPServer):
    pass

## udp_server_handler
#
#   This is the callback for the handling of the requests.
#
class udp_server_handler(SocketServer.BaseRequestHandler):

    ## setup
    #
    #   Just a visual indication of who has connected
    #
    def setup(self):
        self.server.logger.info("Client connected [%s]." % str(self.client_address))
        return

    ## finish
    #
    #   Just a visual indication of who has disconnected
    #
    def finish(self):
        self.server.logger.info("Client disconnected [%s]." % str(self.client_address))
        return

    ## handle
    #
    #   We handle the request
    #
    def handle(self):

        # Get the data
        data = self.request[0].strip()
        self.server.logger.info("Received data in HEX:" + " ".join([hex(ord(x)) for x in data]))
        self.server.logger.info("Received data -> %s" %data)

        # We check to see what is the received command
        if data == START_REMOTE:
            # Lock
            self.server.logger.info("Locking...")
            self.server.lock.acquire()
            self.server.request_state = True
            self.server.logger.info("Starting remote reporting engine")
            self.server.logger.info("Releasing...")
            self.server.lock.release()
            return

        elif data == STOP_REMOTE:
            # Lock
            self.server.logger.info("Locking...")
            self.server.lock.acquire()
            self.server.request_state = False
            self.server.logger.info("Stopping remote reporting engine")
            self.server.logger.info("Releasing...")
            self.server.lock.release()
            return

        else:
            self.server.logger.info("Invalid command received: %s" % data)
            return

## udp_server
#
#   This is the udp server class that handles the RPC requests.
#
class udp_server(threading.Thread):

    # The server address
    address         = ADDRESS

    # The server port
    port            = PORT

    # The server handle
    server          = ""

    # Server Logger
    logger          = ""

    # Contest dictionary
    context_dict    = dict()

    # Thread
    thread          = None

    # State
    request_state   = False

    # Active
    active          = True

    # IPC interface
    ipc_interface   = None

    # Lock object
    lock            = threading.Lock()

    ## __init__
    #
    #   The default constructor for the class
    #
    def __init__(self, address=ADDRESS, port=PORT):

        # Override the thread base class
        threading.Thread.__init__(self)

        # Set the logger
        self.logger = logging.getLogger("udp_server")

        # Set internal variables
        self.address = address
        self.port = port

        # Set the server handle
        self.logger.info("Creating a udp server on: %s:%s" %(str(self.address), str(self.port)))
        self.server = threaded_udp_request_server((self.address, self.port), udp_server_handler)
        self.logger.info("Started UDP server on: %s" %(str(self.server.server_address)))

        # We setup the server instance to link to our state variable
        self.server.request_state = self.request_state
        self.server.logger = self.logger
        self.server.lock = self.lock

        # We initiate the threading process
        t = threading.Thread(target = self.server.serve_forever)
        t.setDaemon(True) # don't hang on exit
        t.start()
        self.logger.info("Started the server thread.")
        return

    ## run
    #
    #   We send all the values that are received via the IPC
    #
    def run(self):

        send_data   = []

        # We operate this thread until active
        while self.active:

            # Lock
            self.lock.acquire()

            # Check is the request is active
            if self.request_state:

                # Send all queued messages
                while not self.ipc_interface.queue.empty():

                    # We get a data piece and we append it to a list to send.
                    data = self.ipc_interface.queue.get()
                    send_data.append(data)

                # We send the data in the JSON form
                self.server.socket.sendall(simplejson.dumps(send_data))
                self.logger.info("UDP Server sent: %s" %simplejson.dumps(send_data))

            # release
            self.lock.release()

        self.logger.info("Killing thread.")
        return

    ## kill
    #
    #   Kills the worker thread
    #
    def kill(self):
        self.active = False
        self.logger.info("Invoked killing signal.")
        return

    ## connect_ipc
    #
    #   Start ipc
    #
    def connect_ipc(self):

        self.logger.info("Starting an IPC client")
        # We set an IPC server interface
        self.ipc_interface = udp_server_ipc()
        self.ipc_interface.start()
        return

    ## disconnect_ipc
    #
    #   Start ipc
    #
    def disconnect_ipc(self):

        self.logger.info("Stopping an IPC client")
        self.ipc_interface.kill()
        return

if __name__ == "__main__":

    from UDP_Server_Logger_Handler import logger

    _logger = logger()
    log = logging.getLogger("test")
    udp = udp_server()
    udp.start()