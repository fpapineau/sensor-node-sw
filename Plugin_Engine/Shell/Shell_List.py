'''
Created on Nov 10, 2014

@author: fpapinea
'''

#####################################################################
######################## IMPORTS ####################################
#####################################################################

import cmd
from Plugin_Engine.Print import print_commands

__author__ = 'francispapineau'

#####################################################################
######################## CONSTANTS ##################################
#####################################################################

# N / A

#####################################################################
######################## CODE #######################################
#####################################################################

## shell_exit_cmd
#
#    The shell process
#
class shell_list_cmd(cmd.Cmd, object):
    
    ##  do_list
    #
    # This lists the loaded plugins
    #
    def do_list(self, arg):
            
        print("\nPlugin categories loaded:")
        for key in self.plugins.keys():
            print("\t" + key)
        return

    ## help_list
    #
    #     Documentation
    #
    def help_list(self):
        print (""" 
                The list command lists all available loaded plugin types.
                
                The <list> command is used as follows:
                    [HomeMon] ->list
            """)
        return

    ##  do_cmdlist
    #
    #   Lists all valid commands
    #
    def do_cmdlist(self, arg):
        print_commands(self.commands)
        return
    
    ## help_cmdlist
    #
    #     Documentation
    #
    def help_cmdlist(self):
        print (""" 
                The cmdlist command lists all available loaded plugin types and there
                respective available commands, unless a plugin is sepcified.
                
                The <cmdlist> command is used as follows:
                    [HomeMon] ->cmdlist [plugin]
            """)
        return