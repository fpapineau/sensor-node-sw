import logging

from JSON_Engine_Logger_Handler import logger
from JSON_File_Observer import json_file_observer
from Server_Clients.JSON_Engine.JSON_Engine_Status_Updater import json_status_updater


logger = logger()
main_logger = logging.getLogger("JSON_main")

# Create a status updater
main_logger.info("creating a status updater.")
updater = json_status_updater()

# Create a json observer
main_logger.info("Creating a json file observer.")
observer = json_file_observer("./JSONs")
