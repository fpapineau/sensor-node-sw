

#####################################################################
######################## IMPORTS ####################################
#####################################################################

import logging
import threading

from XMPP_Client_Handler import xmpp_client_handler
from XMPP_Connection_Settings import xmpp_connection_settings


#####################################################################
######################## CONSTANTS ##################################
#####################################################################
# N/A
#####################################################################
######################## CODE #######################################
#####################################################################
## xmpp_client
#
#   This is the high level xmpp class. It extends the classic object
#   while being in charge of setting up the connection to the server
#   and connecting to the server as well.
#
class xmpp_client(threading.Thread):

    # The internal class logger
    logger = None

    # The client object
    client = None

    # XMPP settings
    xmpp_settings = None

    ## __init__
    #
    #   This is the default constructor to the xmpp_client class
    #
    #   @param xmpp_connection_settings - container for settings
    #
    def __init__(self):
        threading.Thread.__init__(self)
        return

    ## __init__
    #
    #   This is the default constructor to the xmpp_client class
    #
    #   @param xmpp_connection_settings - container for settings
    #
    def set(self, xmpp_connection_settings):

        # Conserve the settings
        self.xmpp_settings = xmpp_connection_settings
        return

    ## start
    #
    #   Starts
    #
    def start(self):
        # Setup sequence
        self.setup_local_loggers()
        self.setup_xmpp_client()
        self.connect_xmpp_client()
        return



    ## setup_local_loggers
    #
    #   This sets up the local internal class loggers.
    #
    def setup_local_loggers(self):

        # We create a new logger
        self.logger = logging.getLogger('xmpp_client')
        self.logger.info('Created local loggers...')
        return

    ## setup_xmpp_client
    #
    #   This sets up the xmpp client and conenctions needed to communicate to the server.
    #
    def setup_xmpp_client(self):

        # Setup the EchoBot and register plugins. Note that while plugins may
        # have interdependence, the order in which you register them does
        # not matter.
        self.client = xmpp_client_handler(self.xmpp_settings.username, self.xmpp_settings.password)
        self.client.register_plugin('xep_0030') # Service Discovery
        self.logger.info('Added the Service Discovery Plugin support.')
        self.client.register_plugin('xep_0004') # Data Forms
        self.logger.info('Added the Data Forms Plugin support.')
        self.client.register_plugin('xep_0060') # PubSub
        self.logger.info('Added the Publish Subscribe Plugin support.')

        self.client.register_plugin('xep_0199') # XMPP Ping
        self.logger.info('Created an xmpp client...')
        return


    ## connect_xmpp_client
    #
    #   This connects to the server
    #
    def connect_xmpp_client(self):

        # Connect to the XMPP server and start processing XMPP stanzas.
        if self.client.connect(use_ssl=False, use_tls=False):

            # If you do not have the dnspython library installed, you will need
            # to manually specify the name of the server if it does not match
            # the one in the JID. For example, to use Google Talk you would
            # need to use:
            #
            # if xmpp.connect(('talk.google.com', 5222)):
            #     ...
            self.client.process(block=False)
            self.logger.debug("Done")
        else:
            self.logger.error("Unable to connect.")
        return


# xmpp_client.py <jid> <secret>
if __name__ == '__main__':

    from XMPP_Constants import *
    from XMPP_Logger_Handler import logger
    import time

    # Create the logger root
    logger = logger()

    # Some splash screen action
    print (xmpp_client_constants.SPLASH_SCREEN + '\n')
    print("Starting up XMPP engine.")
    time.sleep(2)

    # We react to the event
    settings = xmpp_connection_settings('raspi@10.0.1.10/home_monitoring', 'pi')
    xmpp_client(settings)