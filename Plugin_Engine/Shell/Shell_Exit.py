'''
Created on Nov 10, 2014

@author: fpapinea
'''

#####################################################################
######################## IMPORTS ####################################
#####################################################################

import cmd

__author__ = 'francispapineau'

#####################################################################
######################## CONSTANTS ##################################
#####################################################################

# N / A

#####################################################################
######################## CODE #######################################
#####################################################################

## shell_exit_cmd
#
#    The shell process
#
class shell_exit_cmd(cmd.Cmd, object):
    
    ## can_exit
    #
    # Protect the shell
    #
    def can_exit(self):
        return True
    
    ## do_bye
    #
    #   This closes the command shell
    #
    #   @param arg              - the args
    #
    def do_bye(self, arg):

        # Stop recording, close the turtle window, and exit:  BYE
        print('Thank you for using HomeMon')
        self.close()
        return True
    
    ## help_bye
    #
    #     Documentation
    #
    def help_bye(self):
        print (""" 
                The bye command exits the shell context.
                
                The <bye> command is used as follows:
                    [HomeMon] ->bye
            """)
        return
    
    ## do_EOF
    #
    #   This executes when there is a EOF
    #
    #   @param arg              - the args
    #
    def do_EOF(self, args):
        return self.do_exit(args)
    
    ## do_exit
    #
    #   This exits the command shell
    #
    #   @param arg              - the args
    #
    def do_exit(self, args):
        return self.do_bye(args)
    
    ## help_exit
    #
    #     Documentation
    #
    def help_exit(self):
        print (""" 
                The exit command exits the shell context.
                
                The <exit> command is used as follows:
                    [HomeMon] ->exit
            """)
        return