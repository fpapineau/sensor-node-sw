MQTT Home Monitor Server
========================

    - This is a python application which converts the sensor data published on the MQTT
      server and processes them so that the Emoncms Server understands the values.

    - The overall architecture is as follows:

        - Sensor -> MQTT Server -> MQTT Client -> Data Processor -> Emoncms Client -> JSON
                    -> Emoncms Server



    *** NOTE: ***
        - The following are the explanations of what component is what and what their functionality
            are respectively:

                SENSOR:
                - The sensor publishes the sensor information along with system status information
                    to the MQTT server. The sensor also subscribes to the topics that the remote systems
                    might need to reboot the main cpu or their sensor coprocessors.

                MQTT SERVER:
                - This is the centralized server that allows for publishing and subscribing to MQTT topics. In this
                    case we use the open source Mosquitto server.

                MQTT CLIENT:
                - This is the piece of software that stands on the coordinator node. It subscribes to the topics that
                    each sensor node published content to and publishes to the pipes that the sensor node subscribes
                    to.


                        SENSOR NODE                 MQTT SERVER                  MQTT CLIENT

                        PUBLISH -> DATA             -> TOPICS                       -> SUBSCRIBE
                        SUBSCRIBE <-                <- TOPICS                    DATA <- PUBLISH

                DATA PROCESSOR:
                - This is another entity that sits on the coordinator node. It provides a service that, takes in
                    the data that is published to certain topics and parses that data. Then it acts upon it.
                    Either it reacts with a callback method or it then in turn does a POST of that data to the
                    Emoncms Server as a JSON string.

                EMONCMS CLIENT:
                - This is the pice of code that connects to the API hook of the remote Emoncms Server. It provides a
                    static API to publish content to the remote Emoncms Server under specific topics. It wraps the data
                    into a JSON string for easier transport.

                EMONCMS SERVER:
                - This is the receiving server. It receives the client post on a specific pipe and displays the data
                    in a practical and sleek display.


                *** NOTE - OTHER ***
                XMPP SERVER:
                - This is a chat client that processes remote requests on the XMPP Pipe. It parses the XML storage file,
                    and writes the data to the corresponding XMPP Pipe.


                SENSOR HUB FORWARDER:
                - This is strictly a coordinator node process and system monitoring script.


                ORDER OF BOOT
                    - The request server must be up and running before the xmpp server





