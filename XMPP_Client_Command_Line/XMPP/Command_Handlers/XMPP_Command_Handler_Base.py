
#####################################################################
######################## IMPORTS ####################################
#####################################################################

from abc import abstractmethod


#####################################################################
######################## CONSTANTS ##################################
#####################################################################
# N/A
#####################################################################
######################## CODE #######################################
#####################################################################
## xmpp_command_handler
#
#   This is the base interface of a command handler child class.
#   This class provides a standard interface to develop command
#   handlers
#
class xmpp_command_handler(object):

    # The execution pointer
    execution                   = None

    ## setup
    #
    #   The setup method to set the context of the handler.
    #
    @abstractmethod
    def setup(self, executer):
        raise NotImplemented

    ## run
    #
    #   The run method that actually runs the method within the
    #   context setup above.
    #
    def run(self, args):

        command = dict()
        command["CMD"] = args.command
        command["ARGS"] = args.command_args.replace("(", "").replace(")", "")

        # Send the request to the ipc
        self.execution.xmpp_ipc_handle.send_message(command)
        return

    ## stop
    #
    #   This method stops the work of the handler.
    #
    @abstractmethod
    def stop(self):
        raise NotImplemented

    ## destroy
    #
    #   This exits the handler.
    #
    @abstractmethod
    def exit(self):
        raise NotImplemented
