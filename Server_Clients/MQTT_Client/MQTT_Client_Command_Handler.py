
#####################################################################
######################## IMPORTS ####################################
#####################################################################

from MQTT_Client_Control import mqtt_control_commands as commands
from MQTT_Client_Handler_Base import mqtt_client_handler_base


#####################################################################
######################## CONSTANTS ##################################
#####################################################################
# N/A
#####################################################################
######################## CODE #######################################
#####################################################################
## error_handler
#
#   This is the error handler class which implements the base handler
#   interface class
#
class error_handler(mqtt_client_handler_base):

    ## init
    #
    #   This is the system process init method which sets up a task to be done.
    #
    def __init__(self, mqtt_request_handler, topic):

        mqtt_client_handler_base.__init__(self, mqtt_request_handler, topic)
        return

    ## run
    #
    #   This is the run task method, which triggers a run action on the process.
    #
    def run(self):

        ## TOPIC, MESSAGE, QOS, RETAIN
        self.mqtt_handle.publish(self.topic, commands.error, 0, False)
        return

    ## exit
    #
    #   This is the exit method, which triggers an exit action process.
    #
    def exit(self):

        del self
        return
#####################################################################

## boot_handler
#
#   This is the boot handler class which implements the base handler
#   interface class
#
class boot_handler(mqtt_client_handler_base):

    ## init
    #
    #   This is the system process init method which sets up a task to be done.
    #
    def __init__(self, mqtt_request_handler, topic):

        mqtt_client_handler_base.__init__(self, mqtt_request_handler, topic)
        return

    ## run
    #
    #   This is the run task method, which triggers a run action on the process.
    #
    def run(self):

        ## TOPIC, MESSAGE, QOS, RETAIN
        self.mqtt_handle.publish(self.topic, commands.boot, 0, False)
        return

    ## exit
    #
    #   This is the exit method, which triggers an exit action process.
    #
    def exit(self):

        del self
        return
    
#####################################################################

## shutdown_handler
#
#   This is the shutdown handler class which implements the base handler
#   interface class
#
class shutdown_handler(mqtt_client_handler_base):

    ## init
    #
    #   This is the system process init method which sets up a task to be done.
    #
    def __init__(self, mqtt_request_handler, topic):

        mqtt_client_handler_base.__init__(self, mqtt_request_handler, topic)
        return

    ## run
    #
    #   This is the run task method, which triggers a run action on the process.
    #
    def run(self):

        ## TOPIC, MESSAGE, QOS, RETAIN
        self.mqtt_handle.publish(self.topic, commands.shutdown, 0, False)
        return

    ## exit
    #
    #   This is the exit method, which triggers an exit action process.
    #
    def exit(self):

        del self
        return

#####################################################################

## reboot_handler
#
#   This is the reboot handler class which implements the base handler
#   interface class
#
class reboot_handler(mqtt_client_handler_base):

    ## init
    #
    #   This is the system process init method which sets up a task to be done.
    #
    def __init__(self, mqtt_request_handler, topic):

        mqtt_client_handler_base.__init__(self, mqtt_request_handler, topic)
        return

    ## run
    #
    #   This is the run task method, which triggers a run action on the process.
    #
    def run(self):

        ## TOPIC, MESSAGE, QOS, RETAIN
        self.mqtt_handle.mqtt_handle.publish(self.topic, commands.reboot, 0, False)
        return

    ## exit
    #
    #   This is the exit method, which triggers an exit action process.
    #
    def exit(self):

        del self
        return

#####################################################################

## disconnect_handler
#
#   This is the diconnect handler class which implements the base handler
#   interface class
#
class disconnect_handler(mqtt_client_handler_base):

    ## init
    #
    #   This is the system process init method which sets up a task to be done.
    #
    def __init__(self, mqtt_request_handler, topic):

        mqtt_client_handler_base.__init__(self, mqtt_request_handler, topic)
        return

    ## run
    #
    #   This is the run task method, which triggers a run action on the process.
    #
    def run(self):
        ## TOPIC, MESSAGE, QOS, RETAIN
        self.mqtt_handle.mqtt_handle.publish(self.topic, commands.disconnect, 0, False)
        return

    ## exit
    #
    #   This is the exit method, which triggers an exit action process.
    #
    def exit(self):

        del self
        return

#####################################################################

## status_check_handler
#
#   This is the status check handler class which implements the base handler
#   interface class
#
class status_check_handler(mqtt_client_handler_base):

    ## init
    #
    #   This is the system process init method which sets up a task to be done.
    #
    def __init__(self, mqtt_request_handler, topic):

        mqtt_client_handler_base.__init__(self, mqtt_request_handler, topic)
        return

    ## run
    #
    #   This is the run task method, which triggers a run action on the process.
    #
    def run(self):
        ## TOPIC, MESSAGE, QOS, RETAIN
        self.mqtt_handle.mqtt_handle.publish(self.topic, commands.status_check, 0, False)
        return

    ## exit
    #
    #   This is the exit method, which triggers an exit action process.
    #
    def exit(self):

        del self
        return

#####################################################################

## get_handler
#
#   This is the get check handler class which implements the base handler
#   interface class
#
class get_handler(mqtt_client_handler_base):


    ## init
    #
    #   This is the system process init method which sets up a task to be done.
    #
    def __init__(self, mqtt_request_handler, topic):

        mqtt_client_handler_base.__init__(self, mqtt_request_handler, topic)
        return

    ## run
    #
    #   This is the run task method, which triggers a run action on the process.
    #
    def run(self):
        ## TOPIC, MESSAGE, QOS, RETAIN
        self.mqtt_handle.mqtt_handle.publish(self.topic, commands.get_data, 0, False)
        return

    ## exit
    #
    #   This is the exit method, which triggers an exit action process.
    #
    def exit(self):

        del self
        return
    
#####################################################################

## create_command
#
#    This creates a command with the internal command as the working
#    command.
#
def create_command(command):
    
    return
