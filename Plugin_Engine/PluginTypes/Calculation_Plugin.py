__author__ = 'francispapineau'

#####################################################################
######################## IMPORTS ####################################
#####################################################################

from abc import abstractmethod
from Plugin_Engine.PluginTypes.Base_Plugin import base_plugin

#####################################################################
######################## CONSTANTS ##################################
#####################################################################

CONFIG_FILE_FOLDER = './Plugins/'

#####################################################################
######################## CODE #######################################
#####################################################################

## calculation_plugin
#
#   This is a generic calculation plugin class that is used for any
#   app that needs to calculate data onto the command line interface.
#
class calculation_plugin(base_plugin):
    
    # Auto start flag
    start               = False
    
    # The commands available
    required_plugin_commands            = {
                                           "CALC"    : ['calc',     'reset'],
                                           }
    
    ## __init__
    #
    # The default Constructor
    #
    def __init__(self):
        base_plugin.__init__(self, self.start)
        self.valid_commands.append(self.required_plugin_commands)
        return
    
    ## get_data
    #
    #   This gets the data from the XML Engine
    #
    @abstractmethod
    def get_data(self):
        raise NotImplemented

    ## calculate data
    #
    #   This displays the data from the XML Engine
    #
    @abstractmethod
    def calculate_data(self):
        raise NotImplemented

    ## get_valid_commands
    #
    #   This gets all commands that are valid within
    #   this plugin
    #
    @abstractmethod
    def get_valid_commands(self):
        raise NotImplemented