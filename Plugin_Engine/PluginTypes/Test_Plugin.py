__author__ = 'francispapineau'

#####################################################################
######################## IMPORTS ####################################
#####################################################################

from abc import abstractmethod
from Plugin_Engine.PluginTypes.Base_Plugin import base_plugin

#####################################################################
######################## CONSTANTS ##################################
#####################################################################


# N/A


#####################################################################
######################## CODE #######################################
#####################################################################

## test_plugin
#
#    This tests the plugin infrastructure to see if it is active,
#
class test_plugin(base_plugin):

    # Auto start flag
    start                           = True

    # The commands available
    required_plugin_commands        = {
                                        "TESTING" : ['test1', 
                                        'abcdefghijklmnopqrstuvwxyz', 
                                        'test2']
                                       }
    
    ## __init__
    #
    # The default Constructor
    #
    def __init__(self):
        base_plugin.__init__(self, self.start)
        return
