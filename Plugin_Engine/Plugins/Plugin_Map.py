'''
Created on Nov 10, 2014

@author: fpapinea
'''

from Plugin_Engine.Plugins.Emoncms_Client_Plugin import emoncms_client_plugin
from Plugin_Engine.Plugins.Splunk_Client_Plugin import splunk_client_plugin
from Plugin_Engine.Plugins.Test_Client_Plugin import test_client_plugin
from Plugin_Engine.Plugins.XMPP_Client_Plugin import xmpp_client_plugin

PLUGIN_MAP      = {
                   'emoncms_client_plugin'  : emoncms_client_plugin,
                   'splunk_client_plugin'   : splunk_client_plugin,
                   'test_client_plugin'     : test_client_plugin,
                   'xmpp_client_plugin'     : xmpp_client_plugin,
                   }
