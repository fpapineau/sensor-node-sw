'''
Created on Nov 10, 2014

@author: fpapinea
'''

#####################################################################
######################## IMPORTS ####################################
#####################################################################

import cmd
from Plugin_Engine.Print import print_commands

__author__ = 'francispapineau'

#####################################################################
######################## CONSTANTS ##################################
#####################################################################

# N / A

#####################################################################
######################## CODE #######################################
#####################################################################

## help_shell_cmd
#
# The command history process
#
class shell_load_cmd(cmd.Cmd, object):
    
    ## do_load
    #
    #   This method loads a plugin on request
    #
    #   @param arg              - the plugin to load [plugin, path (Null)]
    #
    def do_load(self, arg):

        # Not a valid plugin
        if len(arg) < 2:
            self.do_help(arg)

        # split the args
        args    = arg.split(" ")
        plugin  = args[0]
        path    = args[1]

        # We add the plugin location
        self.manager.setPluginPlaces(self.PLUGIN_BASE_LOCATION.append(path))
        self.manager.collectPlugins()
        print("Loading Plugin %s" %plugin)
        
        plugin_handle = self.manager.getPluginByName(plugin)
        if plugin_handle is None:
            print("Invalid plugin %s" %plugin)
            return
        
        # Print the plugin signatures
        print(("Loading: %s "
                                %plugin_handle.name).ljust(75, '.') +
                                " [ Ok ]\n")

        print(" ***************************************")
        print("  - Name          : %s ", plugin_handle.name)
        print("  - Version       : %s ", plugin_handle.version)
        print("  - Author        : %s ", plugin_handle.author)
        print("  - Signature     : %s ", plugin_handle.plugin_object.get_signature())
        print(" ***************************************")
        print("  - Valid Commands:")
        print_commands(plugin_handle.plugin_object.get_valid_commands())

        self.plugins[plugin_handle.name] = plugin_handle.plugin_object

        # We get the valid commands from the automatic loaded plugins
        temp = plugin_handle.plugin_object.get_valid_commands()
        if temp is not []:
            self.valid_commands[plugin_handle.name] = temp

        # Activate
        self.manager.activatePluginByName(plugin_handle.__class__.__name__)
        
        # We setup the plugin
        plugin_handle.plugin_object.setup()
        
        # We check to see if it has an autostart function
        if(plugin_handle.plugin_object.is_auto_start()):
            plugin_handle.plugin_object.start()


        # We store the plugin loaded
        self.plugins[plugin_handle] = path

        # We call the manager and check to see if the plugin is installed
        # Check if it got loaded
        for plugin_handle in self.manager.getAllPlugins():

            if plugin_handle.plugin_object.__class__.__name__ == args[0]:
                self.load_plugin(plugin_handle.plugin_object.__class__.__name__)

                # Activate
                self.manager.activatePluginByName(plugin_handle.__class__.__name__)
                return True
        return
    
    ## help_load
    #
    #     Documentation
    #
    def help_load(self):
        print (""" 
                The load command takes in plugin name and a path where the plugin
                can be found. Then it loads it within the program context for 
                execution.
                
                The <load> command is used as follows:
                    [HomeMon] ->load <plugin> <path>
            """)
        return

    ## load_plugin
    #
    # This loads the plugin
    #
    @staticmethod
    def load_plugin(plugin_name):
        print(("Loading: " + plugin_name).ljust(75, '.') + " [Ok]")
        return
    
    ## do_load
    #
    #   This method unloads a plugin on request
    #
    #   @param arg              - the plugin to unload [category, plugin]
    #
    def do_unload(self, args):

        # Not a valid plugin
        if len(args) < 1:
            self.do_help(args)

        if args in self.plugins.keys():
            plugin = self.manager.getPluginByName(args)
            plugin.plugin_object.stop()
            self.manager.deactivatePluginByName(args)

            # Unregister the plugin
            self.plugins.pop(args)
            print(("Unloading: " + args + " ").ljust(75, '.') + " [Ok]")
        else:
            print("Invalid plugin %s" %args)
        return
    
    ## help_unload
    #
    #     Documentation
    #
    def help_unload(self):
        print (""" 
                The unload command takes in plugin name and unloads it from the 
                execution context.
                
                The <unload> command is used as follows:
                    [HomeMon] ->unload <plugin>
            """)
        return
    