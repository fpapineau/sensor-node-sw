
#####################################################################
######################## IMPORTS ####################################
#####################################################################

import logging
import pickle
import threading

import Queue
import snakemq
import snakemq.link
from snakemq.message import FLAG_PERSISTENT
import snakemq.message
import snakemq.messaging
import snakemq.packeter
from snakemq.storage.sqlite import SqliteQueuesStorage


#####################################################################
######################## CONSTANTS ##################################
#####################################################################
SOCKET_TIMEOUT      = 100000
XMPP_IPC            = "XMPP"
MQTT_IPC            = "MQTT"
LOCALHOST           = "localhost"
EMPTY               = ''
MQTT_CONNECT_PORT   = 4003
MQTT_LISTEN_PORT    = 4004

#####################################################################
######################## CODE #######################################
#####################################################################

## xmpp_ipc_messenger
#
#   This is the xmq IPC messenger that sends out some command
#   command requests to the zmq server to challenge a response.
#   From this response we then publish it via the XMPP interface.
#
class xmpp_ipc_messenger(threading.Thread):

    # The alive boolean variable.
    alive = True

    # The message builder object
    message_builder = None

    # The message queue that is used to put messages into the message
    # builder's queue
    message_queue   = Queue.Queue()

    # The xmpp handle
    xmpp_handle = None

    # Request server client socket
    request_socket = None

    # The class logger
    logger = None

    ## __init__
    #
    #   The default constructor for the class.
    #
    def __init__(self, xmpp_handle):

        # Create loggers
        self.logger = logging.getLogger('xmpp_client_handler')

        # Init the thread class
        threading.Thread.__init__(self)

        # We save the xmpp handle
        self.xmpp_handle = xmpp_handle

        # We use a socket to send requests to the request server.
        self.logger.info("Creating an IPC request socket...")
        try:

            self.link = snakemq.link.Link()
            self.packeter = snakemq.packeter.Packeter(self.link)
            self.message_storage = SqliteQueuesStorage("../DB/xmpp_storage.db")
            self.messenger = snakemq.messaging.Messaging(XMPP_IPC, EMPTY, self.packeter, self.message_storage)

            # We add the callback method
            self.logger.info("adding a listener callback to the IPC engine")
            self.messenger.on_message_recv.add(self.receive_message)

            # We add the connectors
            self.logger.info("adding connectors to the IPC engine")
            self.link.add_connector((EMPTY, MQTT_LISTEN_PORT))
            self.logger.info("connectors are on ports: XMPP IPC[%d]" %MQTT_LISTEN_PORT)

        except IOError:
            self.logger.error("[ERROR]: Socket IOError, killing thread.")
            self.stop_thread()
        return

    ## send_message
    #
    #   This method sends the request to the zmq server and waits
    #   for a reply.
    #
    def send_message(self, msg):

        self.logger.info("Creating a message to send to the endpoint")

        # Create a message
        message = snakemq.message.Message(pickle.dumps(msg), ttl = 600, flags = FLAG_PERSISTENT)
        self.messenger.send_message(MQTT_IPC, message)
        return

    ## receive_message
    #
    #   This receives a message from the zmq server.
    #
    def receive_message(self, con, ident, message):

        # We await an answer (Must be between 10 secs or timeout)
        self.logger.info("received a message and adding it to the queue")

        # If the response is not none we unpickle it and return
        if message.data is not None:
            unpickled_response = pickle.loads(message.data)
            self.xmpp_handle.send_response("[RESPONSE]: " + str(unpickled_response))
        else:
            self.xmpp_handle.send_response("[RESPONSE]: Socket timeout...")
        return

    ## run
    #
    #   This is the run method for the thread.
    #
    def run(self):

        # We run until the daemon is killed
        while self.alive:
            self.link.loop()
        return

    ## stop_thread
    #
    #   This stops the thread that serves the ipc messenger in
    #   a soft manner.
    #
    def stop_thread(self):
        self.alive = False
        return

    ## create_message
    #
    #   This method enqueue the message passed to it to then create
    #   a message creation job.
    #
    #   *** NOTE: This is the entry point to the API.
    #
    def create_message_job(self, msg):
        self.message_queue.put(msg)
        self.logger.info("Adding a new job to the job queue. Queued jobs: %d" %(self.message_queue.qsize()))
        return