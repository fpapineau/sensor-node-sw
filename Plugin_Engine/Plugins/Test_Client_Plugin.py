__author__ = 'francispapineau'

#####################################################################
######################## IMPORTS ####################################
#####################################################################

import logging
from Plugin_Engine.PluginTypes.Test_Plugin import test_plugin

#####################################################################
######################## CONSTANTS ##################################
#####################################################################
# N/A
#####################################################################
######################## CODE #######################################
#####################################################################
## test_plugin
#
# This the test plugin application.
# It tells us that the engine has started properly.
#
class test_client_plugin(test_plugin, object):

    # Plugin file name
    plugin_file_name = 'Test_Client_Plugin.conf'
    
    # Class signature
    signature       = ""

    # Create a logger object
    logger_object   = logging.getLogger("test_plugin")

    ## __init__
    #
    # The default Constructor
    #
    def __init__(self):
        test_plugin.__init__(self)
        self.set_config_location(self.plugin_file_name)
        return

    ## execute
    #
    #   This is the plugin execution method
    #
    def execute(self, command):
        return "Execution Successful."

    ## get_data
    #
    #   This gets the data from the XML Engine
    #
    def get_data(self):
        self.logger_object.info("Test_Plugin")
        return

    ## display_data
    #
    #   This displays the data from the XML Engine
    #
    def display_data(self):
        self.logger_object.info("Test_Plugin -- display_data")
        return

    ## start_server
    #
    #   starts the client if connected
    #
    def run(self):
        print (self.get_signature())
        return
    
    ## stop
    #
    #   shuts the client down
    #
    def stop(self):
        raise NotImplemented

    ## setup
    #
    #    The worker method.
    #
    def setup(self):
        self.logger_object.info("Test_Plugin -- setup")
        return
