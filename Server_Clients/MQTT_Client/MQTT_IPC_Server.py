#####################################################################
######################## IMPORTS ####################################
#####################################################################

import logging
import pickle
import threading

from MQTT_Client_Function_Table import request_handler_table
from MQTT_IPC_Message_Builder import mqtt_ipc_message_builder
from Queue import Queue
import snakemq.link
from snakemq.message import FLAG_PERSISTENT
import snakemq.message
import snakemq.messaging
import snakemq.packeter
from snakemq.storage.sqlite import SqliteQueuesStorage


#####################################################################
######################## CONSTANTS ##################################
#####################################################################
SOCKET_TIMEOUT      = 100000
XMPP_IPC            = "XMPP"
MQTT_IPC            = "MQTT"
LOCALHOST           = "localhost"
EMPTY               = ''
MQTT_CONNECT_PORT   = 4003
MQTT_LISTEN_PORT    = 4004

#####################################################################
######################## CODE #######################################
#####################################################################

## communication_server
#
#   This is the method of sending and receiving data to and from the
#   client node.
#
class communication_server(threading.Thread):

    # The list containing the server settings.
    server_settings     = None

    # We daemonize the process
    daemon_threads      = True

    # We can reuse the address
    allow_reuse_address = True

    # Logger object
    logger              = None

    # The handler
    mqtt_handle         = None

    # MQTT message queue
    queue               = Queue()

    # Alive
    alive               = True

    # debug
    debug               = False

    # message builder
    message_builder     = None

    ## __init__
    #
    #   The default init constructor
    #
    #
    def __init__(self, mqtt_handle, debug=False):

        # Create a logger
        self.logger = logging.getLogger('communication_server')

        # Init the thread class
        threading.Thread.__init__(self)

        # Assign the mqtt handle
        self.debug = debug
        self.mqtt_handle = mqtt_handle
        self.message_builder = mqtt_ipc_message_builder()

        try:

            self.link = snakemq.link.Link()
            self.packeter = snakemq.packeter.Packeter(self.link)
            self.message_storage = SqliteQueuesStorage("../DB/mqtt_storage.db")
            self.messenger = snakemq.messaging.Messaging(MQTT_IPC, EMPTY, self.packeter, self.message_storage)

            # We add the callback method
            self.logger.info("adding a listener callback to the IPC engine")
            self.messenger.on_message_recv.add(self.receive_message)


            self.logger.info("adding a listener to the IPC engine")
            self.link.add_listener((LOCALHOST, MQTT_LISTEN_PORT))
            self.logger.info("listener are on ports: MQTT IPC[%d]" %MQTT_LISTEN_PORT)
            self.logger.info("Started the MQTT engine ipc")

        except IOError:
            self.logger.error("[ERROR]: Socket IOError, killing thread.")
            self.stop_engine()

    ## stop_engine
    #
    #   This stops the server thread and closes it down
    #
    def stop_engine(self):

        # Close the server thread
        self.alive = False
        return

    ## receive_message
    #
    #   This receives a message to teh appropriate endpoint
    #
    def receive_message(self, conn, ident, message):

        # self.request is the client connection
        data = pickle.loads(message.data)  # clip input at 1Kb
        data = self.build_message(data)

        self.logger.info('Received data %s' %data)

        # The commands are as follows:
        #   - COMMAND : TOPIC

        # We split the command from the args
        data = data.split(':')

        # We check validity
        if not request_handler_table.has_key(data[0]):

            # Not a good request - this will timeout the connection
            request_handler_table['ERROR'](self, data)
            return

        # Check the sensor channel validity
        if not (('control/' in data[1].lower()) and ('_cmd' in data[1].lower())):
            # Not a good request - this will timeout the connection
            request_handler_table['ERROR'](self, data)
            return

        # We then call the appropriate method to execute.
        # We get the request handler and pass the args
        #   - request_handler - args

        self.logger.info('Executing request command: %s' %(data[0]))

        if not self.debug:
            obj = request_handler_table[data[0]](self, data[1])
            status = obj.execute()
            self.logger.info('Execution status: %s' %status)
            # We return the status of the operation
            self.send_message(pickle.dumps(status))
        else :
            self.send_message(pickle.dumps("response"))
        return

    ## build_message
    #
    #   This method calls the message builder to send a command
    #   request to the zmq server.
    #
    def build_message(self, message):

        # We take a message from the queue and pass it to a builder.
        # This call is a blocking call
        message = self.message_builder.build_message(message)
        return message

    ## send_message
    #
    #   This sends a message to teh appropriate endpoint
    #
    #   @param msg                          - the message structure
    #
    def send_message(self, msg):

        self.logger.info("Creating a message to send to the endpoint")
        # Create a message
        message = snakemq.message.Message(msg, ttl = 600, flags = FLAG_PERSISTENT)

        self.logger.info("Sending data to the XMPP IPC server.")
        self.messenger.send_message(XMPP_IPC, message)
        return

    ## run
    #
    #   This starts the engine thread
    #
    def run(self):

        # We loop until killed
        while self.alive:
            self.link.loop()
        return

if __name__ == "__main__":

    import time
    import logging
    from MQTT_Client_Logger_Handler import logger

    logger1 = logger()
    loggg = logging.getLogger("main")


    server = communication_server(None, debug=False)
    server.start()
    while 1:
        time.sleep(1)