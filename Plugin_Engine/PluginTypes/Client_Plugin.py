#####################################################################
######################## IMPORTS ####################################
#####################################################################

from abc import abstractmethod
from Plugin_Engine.PluginTypes.Base_Plugin import base_plugin

#####################################################################
######################## CONSTANTS ##################################
#####################################################################

# N/A

#####################################################################
######################## CODE #######################################
#####################################################################

## client_plugin
#
#   This is a generic client plugin class that is used for any
#   app that needs to transmit data to another process
#
class client_plugin(base_plugin):

    # Auto start flag
    auto_start                          = True

    # The commands available
    required_plugin_commands            = {
                                           "PROCESS"    : ['run',       'stop'],
                                           "CONNECTION" : [ 'connect',  'disconnect']
                                           }
    ## __init__
    #
    # The default Constructor
    #
    def __init__(self):
        base_plugin.__init__(self, self.auto_start)
        self.valid_commands.append(self.required_plugin_commands)
        return

    ## disconnect
    #
    #   disconnects the client and shuts it down
    #
    @abstractmethod
    def disconnect(self):
        raise NotImplemented

    ## connect
    #
    #   Connects the client and starts it up
    #
    @abstractmethod
    def connect(self):
        raise NotImplemented
