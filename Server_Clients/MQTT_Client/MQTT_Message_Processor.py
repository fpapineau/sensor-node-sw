#####################################################################
######################## IMPORTS ####################################
#####################################################################

import logging
import re

import simplejson


#####################################################################
######################## CONSTANTS ##################################
#####################################################################
SUCCESS                         = 0

# The subscripts to the names of the pipes - Ingress
DATA                            = "_d" # ex: Sensor#1_d / Sensor#2_l
LOGS                            = "_l"

# The subscripts for the names of the pipes - Egress
GLOBAL_CONTROL                  = "glb_cmd"
SENSOR_CONTROL                  = "_cmd" # ex: Sensor#3_cmd / gbl_cmd



# The message patterns
MQTT_TIME_HEADER_PATTERN        = "(\[\d+\:\d+\:\d+\])"

# Sensor components
MQTT_SENSOR_TAG_PATTERN         = "(\<[0-9a-zA-Z]+\>)"
MQTT_SENSOR_DATA_PATTERN        = "(\{[a-zA-z\:\d+\.\,\s]+\})"

# All messages
# Sensors
MQTT_DATA_MESSAGE_PATTERN        = "(\*" \
                                  "((\[\d+\:\d+\:\d+\])\:)" \
                                  "(\s(\<[0-9a-zA-Z]+\>))" \
                                  "(\s(\{[a-zA-z\:\d+\.\,\s]+\}))" \
                                  "\*)"

# Log components
MQTT_LOG_LEVEL_TAG_PATTERN      = "(\<[a-zA-Z]+\>)"
MQTT_LOG_MESSAGES_PATTERN       = "(([a-zA-z0-9]+))"

# Logs
MQTT_LOG_MESSAGE_PATTERN       = "(\*" \
                                  "((\[\d+\:\d+\:\d+\])\:)" \
                                  "(\s(\<[a-zA-Z]+\>))" \
                                  "((\s\-\>)" \
                                  "(\s([a-zA-z0-9]+)))" \
                                  "\*)"

# Timer Tag
TIMER_TAG                       = "<DS1307>"

#####################################################################
######################## CODE #######################################
#####################################################################

## mqtt_message_processor
#
#   This is a message processor class. It is a pawned class
#   from the MQTT_Interface class. We use this to allow for
#   message processing without cutting down the cpu speed.
#
class mqtt_message_processor :

    # The class logger
    logger                      = None

    # Data regex
    data_pattern                = None

    # Log regex
    log_pattern                 = None

    # Message timestamp
    timestamp_pattern           = None

    # Sensor tag
    sensor_tag_pattern          = None

    # Data
    sensor_data_pattern         = None

    # Log level
    log_level_tag_pattern       = None

    # log message
    log_msg_pattern             = None

    # Data
    data                        = dict()

    # Device List
    device_list                 = {
                                    "<DS1307>"    : 1,
                                    "<TMP102>"    : 2,
                                    "<TMP006>"    : 3,
                                    "<SHT21>"     : 4,
                                    "<ADS1015>"   : 5,
                                    "<BMP180>"    : 6,
                                    "<GENERIC>"   : 0
                                 }



    # Dictionary for the components
    sensor_components_dict      = {
                                    "NODE"      : None,
                                    "TOPIC"     : None,
                                    "PACKET"    : "DATA PACKET",
                                    "TIME"      : None,
                                    "SENSOR"    : None,
                                    "DATA"      : None
                                    }

    log_components_dict           = {
                                    "TOPIC"     : None,
                                    "PACKET"    : "LOG PACKET",
                                    "TIME"      : None,
                                    "LEVEL"     : None,
                                    "MESSAGE"   : None,
                                    }

    # Internal json engine
    json_engine                     = None

    ## __init__
    #
    #   The default constructor for the class
    #
    #   @param msg                  - the message object
    #   @param json_engine          - the json engine
    #
    def __init__(self, msg, json_engine):

        # Set json engine pointer
        self.json_engine = json_engine

        # Compile the regexes
        # global
        self.log_pattern  = re.compile(MQTT_LOG_MESSAGE_PATTERN)
        self.data_pattern = re.compile(MQTT_DATA_MESSAGE_PATTERN)
        self.timestamp_pattern = re.compile(MQTT_TIME_HEADER_PATTERN)

        # logs
        self.log_level_tag_pattern = re.compile(MQTT_LOG_LEVEL_TAG_PATTERN)
        self.log_msg_pattern = re.compile(MQTT_LOG_MESSAGE_PATTERN)

        # sensor
        self.sensor_data_pattern = re.compile(MQTT_SENSOR_DATA_PATTERN)
        self.sensor_tag_pattern = re.compile(MQTT_SENSOR_TAG_PATTERN)

        # Get a logger object
        self.logger = logging.getLogger("mqtt_message_processor")

        # We set the internal pointer to the processor function
        # We check to see what pipe is needed...
        if DATA in msg.topic:
            self.logger.info("Data received going to process it...")

            # We map the packet to a structure
            self._process_data(msg)

        elif LOGS in msg.topic:
            self.logger.info("Log received going to process it...")

            # We map the packet to a structure
            self._process_log(msg)

        else:
            # Do nothing
            self.logger.info("Nothing important received....")

        return

    # --------------------------------------------------
    # Processing methods

    ## _process_data
    #
    #   This is the processing method for data.
    #
    #   @param msg                 - the message object
    #
    def _process_data(self, msg):

        # Check if valid
        if self.data_pattern.match(msg.payload):

            # Parse out
            self.data = re.match(self.data_pattern, msg.payload).groups()
            fields = list(self.data)

            # Get the topic
            self.sensor_components_dict["TOPIC"] = msg.topic

            # Go over the components
            for item in fields:

                # Get the sensor type
                if re.match(self.sensor_tag_pattern, item):

                    if item in self.device_list.keys():
                        self.sensor_components_dict["SENSOR"] = item
                        self.sensor_components_dict["NODE"] = re.findall("\\d+", msg.topic)[0]
                    else:
                        self.logger.info("Not a valid sensor message")
                        return

                # Get the data
                elif re.match(self.sensor_data_pattern, item):
                    self.sensor_components_dict["DATA"] = item

                # Get the timestamp - sensor
                elif re.match(self.timestamp_pattern, item):
                    self.sensor_components_dict["TIME"] = item

            self.logger.info("Added an json data entry...")
            # Add to XML queue
            if self.sensor_components_dict["SENSOR"] != TIMER_TAG:
                self.json_engine.data_queue.put(simplejson.dumps(str(self.sensor_components_dict)))
            self.json_engine.log_queue.put(simplejson.dumps(str(self.sensor_components_dict)))
        else :
            self.logger.info("Not a well formatted message")
        return

    ## _process_log
    #
    #   This is the processing method for a log message from the
    #   sensor node.
    #
    #   @param msg                  - the message object
    #
    def _process_log(self, msg):

       # Check if valid
        if self.log_pattern.match(msg.payload):

            # Parse out
            self.data = re.match(self.log_pattern, msg.payload).groups()
            fields = list(self.data)

            # Get the topic
            self.log_components_dict["TOPIC"] = msg.topic

            # Go over the components
            for item in fields:

                # Get the sensor type
                if re.match(self.log_level_tag_pattern, item):
                    self.log_components_dict["LEVEL"] = item

                # Get the data
                elif re.match(self.log_msg_pattern, item):
                    self.log_components_dict["MESSAGE"] = item

                # Get the timestamp - sensor
                elif re.match(self.timestamp_pattern, item):
                    self.log_components_dict["TIME"] = item

            self.logger.info("Added an json data entry...")
            # Add to XML queue
            self.json_engine.log_queue.put(simplejson.dumps(self.log_components_dict))
        else :
            self.logger.info("Not a well formatted message")
        return