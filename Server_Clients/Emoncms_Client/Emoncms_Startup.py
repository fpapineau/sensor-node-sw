
#####################################################################
######################## IMPORTS ####################################
#####################################################################

import logging

from Emoncms_Client import emoncms_server_interface
from Emoncms_Client_Logger_Handler import logger


#####################################################################
######################## CONSTANTS ##################################
#####################################################################
API_KEY = "f002505490c99653c7374bcd4cbc3006"

#####################################################################
######################## CODE #######################################
#####################################################################

# Create a logger
top_logger = logger()
log = logging.getLogger("Emoncms Service")

# Create a client
client = emoncms_server_interface(  protocol = 'http://',
                                    domain = '10.0.1.136',
                                    path = '/emoncms',
                                    apikey = API_KEY,
                                    period = 15)

log.info("Starting worker thread")
client.start()
log.info("Started Emoncms service")