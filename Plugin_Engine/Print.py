'''
Created on Nov 10, 2014

@author: fpapinea
'''


## print_commands
#
# Prints commands in a table format
#
def print_commands(commands, logger = None):
    
    if logger is None:
        print("Plugin".ljust(29, ' ') + " | Category".ljust(51, ' ') + " | Commands")
        print("-----".ljust(29, '-') + " | ----".ljust(51, '-') + " | ----")
        for key in commands.keys():
            
            if isinstance(commands[key], dict):
                
                for keys in commands[key].keys():
                    print(("%s" %key).ljust(30, ' ') + 
                          "| %s" %str(keys).ljust(49, ' ') + 
                                      "| %s" %str(commands[key][keys]))
                print("-----".ljust(29, '-') + " | ----".ljust(51, '-') + " | ----")
                    
            else:
                print(("%s" %key).ljust(30, ' ') + 
                          "| %s" %str(key).ljust(49, ' ') + 
                                      "| %s" %str(commands[key]))
                
        print("-----".ljust(29, '-') + " | ----".ljust(51, '-') + " | ----")
        print("\n")
        
    else:
        logger.info("Plugin".ljust(29, ' ') + " | Category".ljust(51, ' ') + " | Commands")
        logger.info("-----".ljust(29, '-') + " | ----".ljust(51, '-') + " | ----")
        for key in commands.keys():
            
            if isinstance(commands[key], dict):
                
                for keys in commands.keys():
                        logger.info(("%s" %key).ljust(30, ' ') + 
                          "| %s" %str(keys).ljust(49, ' ') + 
                                      "| %s" %str(commands[key][keys]))
                logger.info("-----".ljust(29, '-') + " | ----".ljust(51, '-') + " | ----")
                        
            else:
                logger.info(("%s" %key).ljust(30, ' ') + 
                          "| %s" %str(key).ljust(49, ' ') + 
                                      "| %s" %str(commands[key]))
        logger.info("-----".ljust(29, '-') + " | ----".ljust(51, '-') + " | ----")
        return