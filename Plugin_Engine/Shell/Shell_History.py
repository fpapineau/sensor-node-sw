'''
Created on Nov 10, 2014

@author: fpapinea
'''

#####################################################################
######################## IMPORTS ####################################
#####################################################################

import cmd

__author__ = 'francispapineau'

#####################################################################
######################## CONSTANTS ##################################
#####################################################################

# N / A

#####################################################################
######################## CODE #######################################
#####################################################################

## shell_history_cmd
#
# The command history process
#
class shell_history_cmd(cmd.Cmd, object):
    
    # History
    _hist           = []
    
    ## do_hist
    #
    #   This gets the command history
    #
    #   @param arg              - the args
    #
    def do_hist(self, args):
        
        count = 0
        print("\nCommand History:")
        for item in self._hist:
            count += 1
            print("%d".ljust(10) %count + item)

    ## help_hist
    #
    #     Documentation
    #
    def help_hist(self):
        print (""" 
                This command displays the history of commands executed.
                
                The <hist> command is used as follows:
                    [HomeMon] ->hist
                
            """)
        return
    
    ## do_history
    #
    #   This gets the command history
    #
    #   @param arg              - the args
    #
    def do_history(self, args):
        self.do_hist(args)
        return

    ## help_history
    #
    #     Documentation
    #
    def help_history(self):
        print (""" 
                This command displays the history of commands executed.
                
                The <history> command is used as follows:
                    [HomeMon] ->history
                
            """)
        return
