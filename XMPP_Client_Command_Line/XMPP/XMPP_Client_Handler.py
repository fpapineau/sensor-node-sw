

#####################################################################
######################## IMPORTS ####################################
#####################################################################


import logging
from multiprocessing import Lock

from XMPP_Command_Parser import xmpp_command_parser
from XMPP_Constants import *
from XMPP_IPC_Messager import xmpp_ipc_messenger
from XMPP_Job_Creator import xmpp_job_creator
import sleekxmpp


#####################################################################
######################## CONSTANTS ##################################
#####################################################################
# N/A
#####################################################################
######################## CODE #######################################
#####################################################################
## xmpp_client
#
#   This is the xmpp client class which handles the client connection
#   to the xmpp server. It extends the object class.
#
class xmpp_client_handler(sleekxmpp.ClientXMPP):

    # The internal class logger
    logger = None

    # A command parser object
    command_parser = None

    # A job creator
    job_creator = None

    # An IPC interface
    ipc_interface = None

    # The sender for the last message
    sender = None

    # Thread lock
    lock = None

    ## __init__
    #
    #   This is the default constructor for the class
    #
    def __init__(self, jid, password):

        # Create a super object
        sleekxmpp.ClientXMPP.__init__(self, jid, password)

        # Create loggers
        self.logger = logging.getLogger('xmpp_client_handler')

        # We create locks
        self.lock = Lock()

        # The session_start event will be triggered when
        # the bot establishes its connection with the server
        # and the XML streams are ready for use. We want to
        # listen for this event so that we we can initialize
        # our roster.
        self.add_event_handler("session_start", self.start)

        # The message event is triggered whenever a message
        # stanza is received. Be aware that that includes
        # MUC messages and error messages.
        self.add_event_handler("message", self.message)
        self.logger.info("Created a message handler...")

        # Create a parser object
        self.command_parser = xmpp_command_parser()
        self.logger.info("Created a message parser...")

        # Create the IPC Interface
        # Start it
        self.ipc_interface = xmpp_ipc_messenger(self)
        self.ipc_interface.start()
        self.logger.info("Created an IPC Interface...")

        # Create the job creator object
        # Start it
        self.job_creator = xmpp_job_creator(self.ipc_interface, self)
        self.job_creator.start()
        self.logger.info("Created a job handler...")

        return


    ## start
    #
    #   Process the session_start event.
    #
    #   Typical actions for the session_start event are
    #   requesting the roster and broadcasting an initial
    #   presence stanza.
    #
    #   Arguments:
    #       event -- An empty dictionary. The session_start
    #              event does not provide any additional
    #              data.
    def start(self, event):

        self.send_presence()
        self.get_roster()
        self.logger.info('Started connection...')
        self.logger.info('Client running...')
        return


    ## message
    #
    #   Process incoming message stanzas. Be aware that this also
    #   includes MUC messages and error messages. It is usually
    #   a good idea to check the messages's type before processing
    #   or sending replies.
    #
    #   Arguments:
    #       msg -- The received message stanza. See the documentation
    #               for stanza objects and the Message stanza to see
    #               how it may be used.
    def message(self, msg):

        if msg['type'] in ('chat', 'normal'):
            if msg['from'] != xmpp_client_constants.SERVER_ADDRESS:

                self.logger.info('Message received from : %s:' %msg['from'])
                self.logger.info('MSG: %s' %msg['body'])

                # Loopback for testing purposes only.
                #msg.reply("Thanks for sending\n%(body)s" %msg).send()

                # We get only the body of the message
                if self.command_parser.parse_message('%(body)s' %msg):
                    self.send_message(msg['from'], '[ERROR]')
                    self.reply_error(msg)
                else:
                    self.send_message(msg['from'], '[OK]')
                    self.sender = msg['from']
        return

    ## reply_error
    #
    #   This message replies with an error
    #
    #   @param msg      - the message handle
    #
    def reply_error(self, msg):

        self.send_message(msg['from'], xmpp_client_constants.ERROR)
        return

    ## send_response
    #
    #   This is the accessible method to write to the xmpp pipe.
    #   This method uses the multiprocessing locks to make sure no 2 threads
    #   accesses this pipe at the same time.
    #
    #   @param response                 - the message to be sent
    #
    def send_response(self, response):

        # Send message
        self.lock.acquire()
        self.send_message(self.sender, response)
        self.lock.release()
        return