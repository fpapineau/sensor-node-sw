

#####################################################################
######################## IMPORTS ####################################
#####################################################################

from MQTT_Client_Command_Handler import *


#####################################################################
######################## CONSTANTS ##################################
#####################################################################
# N/A
#####################################################################
######################## CODE #######################################
#####################################################################
# This is a packet handler pointer table, where the packet type is on
# the left hand side and the function pointer is on the left.
request_handler_table = {

    ''              : 0,
    'BOOT'          : reboot_handler, #
    'REBOOT'        : reboot_handler, #
    'DISCONNECT'    : disconnect_handler, #
    'SHUTDOWN'      : shutdown_handler, #
    
    'ERROR'         : error_handler, 
    
    'STATUS'        : status_check_handler,
    'GET'           : get_handler #
}