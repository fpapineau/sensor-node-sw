
#####################################################################
######################## IMPORTS ####################################
#####################################################################

from abc import abstractmethod

import Queue


#####################################################################
######################## CONSTANTS ##################################
#####################################################################
# N/A
#####################################################################
######################## CODE #######################################
#####################################################################
## mqtt_client_handler_base
#
#   This is the MQTT Client handler base interface class
#
class mqtt_client_handler_base(object):

    # How the process finished overall.
    #   - 1 = fail / 0 = success
    process_status          = 0

    # Mqtt handle
    mqtt_handle             = None

    # Topic to publish to
    topic                   = None

    # Response queue
    queue                   = Queue.Queue()

    ## __init__
    #
    #   The default constructor
    #
    def __init__(self, mqtt_handle, topic):

        # Set the internals
        self.mqtt_handle = mqtt_handle
        self.topic = topic
        return

    ## run
    #
    #   This is the run task method, which triggers a run action on the process.
    #
    @abstractmethod
    def run(self):
        raise NotImplemented

    ## exit
    #
    #   This is the exit method, which triggers an exit action process.
    #
    @abstractmethod
    def exit(self):
        raise NotImplemented

    ## execute
    #
    #   This is the run method, which triggers an run action process.
    #
    def execute(self):

        data = ""

        # Response is now a command response type
        self.mqtt_handle.command_response = True

        self.run()
        self.exit()

        # Get the data within a 20 second span
        if not self.mqtt_handle.mqtt_handle.command_queue.empty():
            data = self.mqtt_handle.mqtt_handle.command_queue.get(timeout = 20)


            # Block other data from being command responses
        self.mqtt_handle.command_response = False

        # We check if we got a message back
        if data is not "":
            return data
        else:
            return '[ERROR]: MQTT Server timeout.'
