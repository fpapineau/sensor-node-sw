
#####################################################################
######################## IMPORTS ####################################
#####################################################################

import logging
import threading

import Queue
import snakemq.link
import snakemq.message
import snakemq.messaging
import snakemq.packeter


#####################################################################
######################## CONSTANTS ##################################
#####################################################################
JSON_TITLE          = "JSON_CREATOR"
EMONCMS_TITLE       = "EMONCMS_SERVER"
SPLUNK_TITLE        = "SPLUNK_SERVER"
LOCALHOST           = ""

EMONCMS_PORT        = 4000
SPLUNK_PORT         = 4001

MESSAGE_TYPE_DATA   = 'D'
MESSAGE_TYPE_LOG    = 'L'

#####################################################################
######################## CODE #######################################
#####################################################################

## splunk_client_ipc
#
#   This is the driver class for the IPC between the JSON engine and
#   the Emoncms, Splunk servers.
#
class splunk_client_ipc(threading.Thread):

    # The class logger
    logger                          = None

    # The class link
    link                            = None

    # The class packeter
    packeter                        = None

    # The class messenger
    messenger                       = None

    # The class message storage
    message_storage                 = None

    # The internal loop lock
    lock                            = True

    # Internal queue
    queue                           = Queue.Queue()

    ## __init__
    #
    #   The default constructor for the class
    #
    def __init__(self):

        # Override the thread constructor
        threading.Thread.__init__(self)

        # Get a logger
        self.logger = logging.getLogger("splunk_client_ipc")

        # Set the internals
        self.logger.info("creating a ipc server.")
        self.link = snakemq.link.Link()
        self.packeter = snakemq.packeter.Packeter(self.link)
        self.messenger = snakemq.messaging.Messaging(SPLUNK_TITLE, LOCALHOST, self.packeter)

        # We add the connectors
        self.logger.info("adding a listener to the IPC engine")
        self.link.add_listener((LOCALHOST, SPLUNK_PORT))
        self.logger.info("listener is on ports: Splunk [%d] " %SPLUNK_PORT)

        # We add the callback method
        self.logger.info("adding a listener callback to the IPC engine")
        self.messenger.on_message_recv.add(self.receive_message)

        self.logger.info("Starting the Splunk engine ipc")
        self.start()
        return

    ## receive_message
    #
    #   This receives a message to teh appropriate endpoint
    #
    def receive_message(self, conn, ident, message):

            self.logger.info("received a message and adding it to the queue")
            # We put a message in the queue
            self.queue.put(message, block=True)
            return

    ## start
    #
    #   This starts the engine thread
    #
    def run(self):

        # We loop until killed
        while self.lock:
            self.link.loop()
        return

    ## kill
    #
    #   Kill the thread
    #
    def kill(self):
        self.lock = False
        return