
#####################################################################
######################## IMPORTS ####################################
#####################################################################

# N/A

#####################################################################
######################## CONSTANTS ##################################
#####################################################################

# N/A

#####################################################################
######################## CODE #######################################
#####################################################################


####### XMPP CONNECTION SETTINGS #########

## xmpp_connection_settings
#
#   This is the connection settings objects that will hold
#   the server credentials and the connections settings for the
#   server.
#
class xmpp_connection_settings(object):

    # The server credentials
    jid             = None
    password        = None

    # Max threads that can be spawned
    max_threads     = None

    ## __init__
    #
    #   This is the default constructor for the connection
    #   settings object.
    #
    #   @param server_address   - the address of the server
    #   @param server_port      - the access port for the server
    #
    #   @param username         - the user for the xmpp server
    #   @param password         - the user password
    #
    def __init__(self, jid, password, max_threads=3):

        # We assign the info needed
        self.username = jid
        self.password = password
        self.max_threads = max_threads
        return

    ## __delete
    #
    #   This is the delete instance method.
    #
    #   @param instance         - the instance passed
    #
    def __delete__(self, instance):
        del instance
        return
