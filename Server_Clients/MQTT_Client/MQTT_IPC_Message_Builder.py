
#####################################################################
######################## IMPORTS ####################################
#####################################################################

import json
import pickle


#####################################################################
######################## CONSTANTS ##################################
#####################################################################
# N/A
#####################################################################
######################## CODE #######################################
#####################################################################
## xmpp_ipc_message_builder
#
#   This is the IPC request message builder class. It basically builds
#   a JSON request command from the message that is sent to the queue.
#
class mqtt_ipc_message_builder(object):

    ## __init__
    #
    #   The default constructor for the class.
    #   This is a static class no constructor is needed
    #
    def __init__(self):
        pass

    ## build_message
    #
    #   This is the build message method.
    #
    @staticmethod
    def build_message(msg):

        # We take the command type passed to this method
        # and we convert it as follows
        #   - DICT
        #   - JSON
        #   - PICKLE

        temp = msg.values()
        temp = str(temp).replace("'", "")\
                        .replace("[", "")\
                        .replace("]", "")\
                        .replace(",", ":")\
                        .replace(" ", "")
        return temp