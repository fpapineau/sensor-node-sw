__author__ = 'francispapineau'

#####################################################################
######################## IMPORTS ####################################
#####################################################################

import sys
import threading

from Plugin_Engine.PluginTypes.Client_Plugin import client_plugin
from XMPP_Client_Command_Line.XMPP.XMPP_Client import xmpp_client, xmpp_connection_settings

#####################################################################
######################## CONSTANTS ##################################
#####################################################################

# N/A

#####################################################################
######################## CODE #######################################
#####################################################################
## calculation_plugin
#
#   This is a generic calculation plugin class that is used for any
#   app that needs to calculate data onto the command line interface.
#
class xmpp_client_plugin(client_plugin, object):

    # Plugin file name
    plugin_file_name = 'XMPP_Client_Plugin.conf'

    # Client handle
    handle              = None

    # xmpp
    xmpp                = None

    ## __init__
    #
    # The default Constructor
    #
    def __init__(self):
        self.auto_start = True
        client_plugin.__init__(self)
        return

    ## setup
    #
    #    The worker method.
    #
    def setup(self):

        self.set_config_location(self.plugin_file_name)

        configs = self.read_configs()
        if configs is not None:
            string = "Booting XMPP client engine ".ljust(75, '.')
            try:

                # Loading the driver
                settings = xmpp_connection_settings(
                    configs.get("XMPP Application Configuration", "Username"),
                    configs.get("XMPP Application Configuration", "Password")
                )
                self.xmpp = xmpp_client()
                self.xmpp.set(settings)

            except Exception as e:
                print(e)
                string += ' [ Failed ]'
                self.logger_object.error(string)
                self.logger_object.info("Exiting main context.")
                sys.exit(1)
            return
        else:
            self.logger_object.error("No configs loaded.")
        return

    # ------- Thread Level --------

    ## start_server
    #
    #   starts the client if connected
    #
    def run(self):
        self.logger_object.info("Starting the plugin.")
        self.xmpp.start()
        pass

    ## stop
    #
    #   shuts the client down
    #
    def stop(self):
        self.logger_object.info("Stopping the plugin.")
        self.handle.client.stop()
        return

    # ------- Connection Level --------

    ## disconnect
    #
    #   disconnects the client and shuts it down
    #
    def disconnect(self):
        self.logger_object.info("Disconnecting the plugin.")
        self.handle.client.disconnect()
        return

    ## connect
    #
    #   Connects the client and starts it up
    #
    def connect(self):
        self.logger_object.info("Connecting the plugin.")
        pass
