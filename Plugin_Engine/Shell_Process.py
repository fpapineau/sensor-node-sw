#####################################################################
######################## IMPORTS ####################################
#####################################################################

from __future__ import print_function
from Plugin_Engine.Shell.Shell_Execute import shell_execute_cmd
from Plugin_Engine.Shell.Shell_Exit import shell_exit_cmd
from Plugin_Engine.Shell.Shell_Help import shell_help_cmd
from Plugin_Engine.Shell.Shell_History import shell_history_cmd
from Plugin_Engine.Shell.Shell_List import shell_list_cmd
from Plugin_Engine.Shell.Shell_Load import shell_load_cmd
from Plugin_Engine.Shell.Shell_Playback import shell_playback_cmd
from Plugin_Engine.Shell.Shell_Record import shell_record_cmd
from Plugin_Engine.Shell.Shell_Shell import shell_shell_cmd

import cmd

__author__ = 'francispapineau'

#####################################################################
######################## CONSTANTS ##################################
#####################################################################

# Plugin locations
PLUGIN_BASE_LOCATION = ['./Plugin_Engine/Plugins']
PLUGIN_CATEGORIES    = ["servers", "clients", "reporting"]

#####################################################################
######################## CODE #######################################
#####################################################################

## plugin_commands
#
#   This is the commands that are available for the command line applications.
#
class shell(shell_execute_cmd,
            shell_exit_cmd,
            shell_help_cmd, 
            shell_history_cmd, 
            shell_list_cmd,
            shell_load_cmd,
            shell_playback_cmd,
            shell_record_cmd,
            shell_shell_cmd):

    # File name
    file            = None

    # Local
    _locals         = {}

    # Globals
    _globals        = {}

    # Command Introduction
    introduction    = "[HomeMon]: Welcome to the command HomeMon shell." \
                      "\tType help or ? to list commands.\n"

    # Command Prompt
    prompt          = "[HomeMon] ->"

    # Plugin manager
    manager         = None

    # Valid commands
    commands        = {}
    
    # plugins loaded
    plugins         = {}

    ## __init__
    #
    #   The class constructor.
    #
    def __init__(self, manager, commands, auto_loaded):

        # Setup the cmd server
        cmd.Cmd.__init__(self)
        self.commands = commands
        self.manager = manager
        self.plugins = auto_loaded
        
        # We check the manager part first
        return

    # ----- Override methods in Cmd object ----- 

    ## run
    #
    #   Default work method
    #
    def run(self):
        self.cmdloop(self.introduction)
        return

    ##  parse
    #
    #   This is the parsing function for any args passed to it
    #
    def parse(self, arg):

        # Convert a series of zero or more numbers to an argument tuple
        return tuple(map(int, arg.split()))
    
    ## preloop
    #
    #   Initialization before prompting user for commands.
    #   Despite the claims in the Cmd documentation, Cmd.preloop() is not a stub.
    #
    def preloop(self):
        cmd.Cmd.preloop(self)   # sets up command completion
        self._hist    = []      # No history yet
        self._locals  = {}      # Initialize execution namespace for user
        self._globals = {}
        return
    
    ## postloop
    #
    #   Take care of any unfinished business.
    #   Despite the claims in the Cmd documentaion, Cmd.postloop() is not a stub.
    #
    def postloop(self):
        cmd.Cmd.postloop(self)   ## Clean up command completion
        print ("Exiting...")

    ## precmd
    #
    #   Precmd utility
    #
    #   @param arg              - the line
    #
    def precmd(self, line):

        # Convert to lower case
        line = line.lower()

        # If the file exists and its not playback time
        if self.file and 'playback' not in line:
            print(line, file=self.file)

        # Add to history
        self._hist += [line.strip()]
        return line

    ## postcmd
    #
    #   If you want to stop the console, return something that evaluates to true.
    #   If you want to do some post command processing, do it here.
    #
    #   @param line - the line with all args
    #   @param stop - the stop bool
    #
    def postcmd(self, stop, line):
        return stop

    ## emptyline
    #
    #   Do nothing on empty input line
    #
    def emptyline(self):
        pass

    ## default
    #
    #   Called on an input line when the command prefix is not recognized.
    #   In that case we execute the line as Python code.
    #
    def default(self, line):

        try:
            exec(line) in self._locals, self._globals
        except Exception:
            print ('No command found Exception')
            self.help_help()
        return
    
    ## close
    #
    #   Record the commands to a specified file
    #
    #   @param arg              - the file name
    #
    def close(self):
        if self.file:
            self.file.close()
            self.file = None
        return