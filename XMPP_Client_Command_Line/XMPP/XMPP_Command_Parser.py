
#####################################################################
######################## IMPORTS ####################################
#####################################################################

import logging
import time

import Queue
from XMPP_Client_Command_Line.XMPP.XMPP_Command import xmpp_command


#####################################################################
######################## CONSTANTS ##################################
#####################################################################
ARGS_INDEX = 1

#####################################################################
######################## CODE #######################################
#####################################################################

## xmpp_command_parser
#
#   This class is the command parser. The first interaction
#   layer into the XMPP command responder application. It extends
#   the queue class, as it formats the packet appropriately and puts
#   it into the command queue.
#
class xmpp_command_parser(object):


    # The arguments splitter
    SPLITTER = ' '

    # Define the queues
    action_command_queue = None
    request_command_queue = None

    # List of slitted command components
    split_command = []

    # The command that will be created.
    command = None

    # The logger
    logger = None

    ## __init__
    #
    #   The default constructor of the class.
    #
    def __init__(self):

        # We create the queue objects
        self.action_command_queue = Queue.Queue()
        self.request_command_queue = Queue.Queue()

        # Create a logger
        self.logger = logging.getLogger('xmpp_command_parser')
        return


    ## parse_message
    #
    #   This method parses the messa ge sent via the XMPP interface.
    #   The message will be in the following form:
    #
    #   Form:
    #       - [Request][Value Index][Args]
    #       - [Action][Command][Args]
    #
    def parse_message(self, msg):

        # We parse the command components
        # ex: Action Shutdown Delay=10
        self.split_command = str(msg).upper().split(self.SPLITTER)

        # If we have an action command
        if self.split_command[0] == 'ACTION' or self.split_command[0] == 'ACT':
            if self.check_valid_command():
                self.parse()
                self.command.set_command_id(self.action_command_queue.qsize())
                self.add_to_action_queue()
            else:
                return 1

        # If we have a request command
        elif self.split_command[0] == 'REQUEST' or self.split_command[0] == 'REQ':
            if self.check_valid_command():
                self.parse()
                self.command.set_command_id(self.request_command_queue.qsize())
                self.add_to_request_queue()
            else:
                return 1
        else:
            return 1

        return 0

    ## check_valid_command
    #
    #   This method checks if the command is valid. It checks
    #   if there is any command parameter in the received packet.
    #
    def check_valid_command(self):

        # Iterate through the array of params and check if
        # there is a command parameter
        for item in self.split_command:
            if item.startswith('CMD='):
                self.logger.info("Received a valid command")
                return True
        self.logger.info("Received a invalid command")
        return False

    ## parse
    #
    #   This method parses the parameters from the command and creates
    #   a command object.
    #
    def parse(self):

        # Assign temp variables
        command = None
        delay = None
        args = None

        # Parse the command received
        # Then we check the command itself
        for item in self.split_command:

            if item.startswith('CMD='):
                command = item.split('=')[ARGS_INDEX]

            elif item.startswith('DELAY='):
                delay = item.split('=')[ARGS_INDEX]

            elif item.startswith('ARGS='):
                args = item.split('=')[ARGS_INDEX]

        # Create a timestamp
        timestamp = time.strftime('%H:%M:%S')

        # We create a command object to contain the command sent
        self.command = xmpp_command(delay, timestamp, args, command, command_alive=True)
        return

    ## add_to_action_queue
    #
    #   This adds an object to the action queue
    #
    def add_to_action_queue(self):
        self.action_command_queue.put(self.command, block=True)
        self.logger.info('Action found and added to the command queue.')
        return

    ## add_to_request_queue
    #
    #   This adds an object to the request queue
    #
    def add_to_request_queue(self):
        self.request_command_queue.put(self.command, block=True)
        self.logger.info('Request found and added to the command queue.')
        return