__author__ = 'francispapineau'

#####################################################################
######################## IMPORTS ####################################
#####################################################################

import ConfigParser
import logging
import os
import sys

from Server_Clients.MQTT_Client.MQTT_Client import mqtt_client
from Server_Clients.JSON_Engine.JSON_File_Observer import json_file_observer
from Server_Clients.JSON_Engine.JSON_Engine_Status_Updater import json_status_updater

#####################################################################
######################## CONSTANTS ##################################
#####################################################################

# n/a

#####################################################################
######################## CODE #######################################
#####################################################################

## system_context
#
# Starts System Context
#
class system_context(object):

    # Plugin file name
    plugin_file_name    = 'System_Plugin.conf'

    # Override the auto start flag
    auto_start          = True

    # No commands available
    required_plugin_commands = ""

    # configs
    config_file = None

    # Client handle
    mqtt_handle         = None
    json_engine_handle  = None
    json_updater_handle = None

    # Set the config reader
    configs = ConfigParser.ConfigParser()

    # Create a logger object
    logger_object   = logging.getLogger("system_context")

    ## setup
    #
    #    The worker method.
    #
    def setup(self):

        # Execute the rm for files
        self.logger_object.info("Removing the previous JSON files if there are any.")
        os.system("rm ./JSONS/*.json")
        os.system("rm ./JSONS/*.parsed")

        self.config_file = "./System_Plugin.conf"
        configs = self.read_configs()

        if configs is not None:
            string = "Booting system context".ljust(75, '.')
            self.logger_object.info("Setup the system cores.")

            # We start the engines
            self.logger_object.info("Starting system engines.")

            self.mqtt_handle = None

            # MQTT Client
            string = "Booting MQTT client engine ".ljust(75, '.')
            try:
                self.mqtt_handle = mqtt_client(
                    configs.get("MQTT Application Configuration", "Id"),
                    configs.get("MQTT Application Configuration", "Port"),
                    configs.get("MQTT Application Configuration", "Host")
                )

            except Exception:

                string += ' [ Failed ]'
                self.logger_object.error(string)
                self.logger_object.info("Exiting main context.")
                sys.exit(1)

            self.mqtt_handle.start()

            string += ' [ Ok ]'
            self.logger_object.info(string)

            # JSON Client
            string = "Booting JSON engine ".ljust(75, '.')
            try:
                self.json_engine_handle = json_file_observer('../JSONS/')
                self.json_updater_handle = json_status_updater()

            except Exception:
                string += ' [ Failed ]'
                self.logger_object.error(string)
                self.logger_object.info("Exiting main context.")
                sys.exit(1)

            string += ' [ Ok ]'
            self.logger_object.info(string)
            return
        else:
            self.logger_object.error("No configs loaded.")
        return

    ## read_configs
    #
    #   We read the configs
    #
    def read_configs(self):

        # We get the configs
        self.logger_object.info("Reading the configs")

        # Read the configs
        self.configs.read(self.config_file)

        # Check for valid sections and values
        if self.configs.sections() > 0 :
            return self.configs
        else:
            return None
