#####################################################################
######################## IMPORTS ####################################
#####################################################################

import ast
import logging
import threading
import time

from Emoncms_Client_Logger_Handler import logger
from Emoncms_Data_Package import emoncms_data_package
from Emoncms_IPC import emoncms_client_ipc
import httplib
import simplejson
import urllib2


#####################################################################
######################## CONSTANTS ##################################
#####################################################################
API_KEY = "f002505490c99653c7374bcd4cbc3006"
TRUE                = 0

#####################################################################
######################## CODE #######################################
#####################################################################

## emoncms_server_interface
#
#   The emoncms server interface. That constructs the emoncms REST API
#   get and put interface to the server.
#
class emoncms_server_interface(threading.Thread):

    # Internal lock
    lock                        = True

    # Internal IPC Server
    ipc_server                  = None

    # Packages
    packages                    = []

    # Data structure
    data                        = {
                                "NODE": None,
                                "DATA": ""
                                }

    ## __init__
    #
    #   The default constructor for the class.
    #
    # @param protocol (string):  "https://" or "http://"
	# @param domain   (string):  domain name (eg: 'domain.tld')
	# @param path     (string):  emoncms path with leading slash (eg: '/emoncms')
	# @param apikey   (string):  API key with write access
	# @param period   (int):     sending interval in seconds
    #
    def __init__(self, protocol, domain, path, apikey):

        threading.Thread.__init__(self)

        self._protocol = protocol
        self._domain = domain
        self._path = path
        self._apikey = apikey
        self._data_buffer = []
        self._last_send = time.time()
        self._logger = logging.getLogger("emoncms_server_interface")
        self._logger.info("Emoncms context loaded.")
        return
 
    ## add_data
    #
    #   This adds data to the server buffer
    #   Append timestamped dataset to buffer.
    #
    # @param data (list): node and values (eg: '[node,val1,val2,...]')
    #
    def add_data(self, data):
		
        self._logger.info("Server " + self._domain + self._path + " -> add data: " + str(data))
        self._data_buffer.append(data)
        return

    ## connect_ipc
    #
    #   Start ipc
    #
    def connect_ipc(self):

        self._logger.info("Starting an IPC client")
        self.ipc_server = emoncms_client_ipc()
        self.ipc_server.setDaemon(True)
        self.ipc_server.start()
        return

    ## disconnect_ipc
    #
    #   Start ipc
    #
    def disconnect_ipc(self):

        self._logger.info("Stopping an IPC client")
        self.ipc_server.kill()
        return

    ## send_data
    #
    #   This sends the data to the emoncms server
    #
    def send_data(self):

        for item in self._data_buffer:

            data_string = item
            self._logger.debug("Data string: " + data_string)

            # Prepare URL string of the form
            # 'http://domain.tld/emoncms/input/bulk.json?apikey=12345&data=[[-10,10,1806],[-5,10,1806],[0,10,1806]]'
            url_string = self._protocol + self._domain + self._path + "/node/set.json" + data_string + "?apikey=" + self._apikey
            self._logger.info("URL string: " + url_string)

            # Send data to server
            self._logger.info("Sending to " + self._domain + self._path)
            try:
                result = urllib2.urlopen(url_string)
            except urllib2.HTTPError as e:
                self._logger.warning("Couldn't send to server, HTTPError: " + str(e.code))
            except urllib2.URLError as e:
                self._logger.warning("Couldn't send to server, URLError: " + str(e.reason))
            except httplib.HTTPException:
                self._logger.warning("Couldn't send to server, HTTPException")
            except Exception:
                import traceback
                self._logger.warning("Couldn't send to server, Exception: " + traceback.format_exc())
            else:
                if result.readline() == 'ok':
                    self._logger.info("Send ok")
                else:
                    self._logger.warning("Send failure")

        # Update _last_send
        self._data_buffer = []
        self._last_send = time.time()
        return

    ## has_data
    #
    #   Check if buffer has data
    #   Return True if data buffer is not empty.
    #
    def has_data(self):
        return self._data_buffer != []

    ## run
    #
    #   Start the engine thread
    #
    def run(self):

        # While run lock is true
        while self.lock:

            data_list = []
            data_dict = dict()
            package = None

            # We check the JSON parser.
            # We get the dicts fromt he internal queue
            while not self.ipc_server.queue.empty():

                # Get a piece of the puzzle
                data = self.ipc_server.queue.get()
                data_dict = ast.literal_eval(simplejson.loads(data.data))

                exists = False
                # We check to see if we have that packages within the class
                for item in self.packages:

                    if item.get_node() == data_dict['NODE']:
                        item.append_data(data_dict)
                        exists = True

                # If that packages does not exist then create one and append it
                if not exists:
                    package = emoncms_data_package()
                    package.append_data(data_dict)
                    self.packages.append(package)


                # Check for valid items
                for item in self.packages:
                    if item.check_valid() == TRUE:
                        self.add_data(item.get_data())

                        # Remove that package from the list
                        self.packages.remove(item)

                if len(self._data_buffer) > 0:
                    self.send_data()
        return

    ## kill
    #
    #   Kill the thread
    #
    def kill(self):
        self.lock = False
        return