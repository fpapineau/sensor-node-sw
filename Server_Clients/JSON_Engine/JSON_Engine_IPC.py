
#####################################################################
######################## IMPORTS ####################################
#####################################################################

import logging
import threading

import snakemq.link
from snakemq.message import FLAG_PERSISTENT
import snakemq.message
import snakemq.messaging
import snakemq.packeter
from snakemq.storage.sqlite import SqliteQueuesStorage


#####################################################################
######################## CONSTANTS ##################################
#####################################################################
JSON_TITLE          = "JSON_CREATOR"
EMONCMS_TITLE       = "EMONCMS_SERVER"
SPLUNK_TITLE        = "SPLUNK_SERVER"
UDP_TITLE           = "UDP_SERVER"
LOCALHOST           = ""

EMONCMS_PORT        = 4000
SPLUNK_PORT         = 4001
UDP_PORT            = 4002

MESSAGE_TYPE_DATA   = 'D'
MESSAGE_TYPE_LOG    = 'L'

#####################################################################
######################## CODE #######################################
#####################################################################

## json_engine_ipc
#
#   This is the driver class for the IPC between the JSON engine and
#   the Emoncms, Splunk servers.
#
class json_engine_ipc(threading.Thread):

    # The class logger
    logger                          = None

    # The class link
    link                            = None

    # The class packeter
    packeter                        = None

    # The class messenger
    messenger                       = None

    # The class message storage
    message_storage                 = None

    # The internal loop lock
    lock                            = True

    # snakemq
    snake                           = None

    ## __init__
    #
    #   The default constructor for the class
    #
    def __init__(self):

        # Override the thread constructor
        threading.Thread.__init__(self)

        # Get a logger
        self.logger = logging.getLogger("json_engine_ipc")
        snakemq.init_logging(self.logger)

        # Set the internals
        self.logger.info("creating a ipc server.")
        self.link = snakemq.link.Link()
        self.packeter = snakemq.packeter.Packeter(self.link)
        self.message_storage = SqliteQueuesStorage("../DB/json_storage.db")
        self.messenger = snakemq.messaging.Messaging(JSON_TITLE, LOCALHOST, self.packeter, self.message_storage)

        # We add the connectors
        self.logger.info("adding connectors to the IPC engine")
        self.link.add_connector((LOCALHOST, EMONCMS_PORT))
        self.link.add_connector((LOCALHOST, SPLUNK_PORT))
        self.link.add_connector((LOCALHOST, UDP_PORT))
        self.logger.info("connectors are on ports: Emoncms [%d] Splunk [%d] UDP [%d]"
                         %(EMONCMS_PORT, SPLUNK_PORT, UDP_PORT))

        self.logger.info("Starting the json engine ipc")
        self.start()
        return

    ## send_message
    #
    #   This sends a message to teh appropriate endpoint
    #
    #   @param msg                          - the message structure
    #
    def send_message(self, msg):

        self.logger.info("Creating a message to send to the endpoint")
        # Create a message
        message = snakemq.message.Message(msg.msg, ttl = 600, flags = FLAG_PERSISTENT)

        # Check to see if the message is data or logs
        if msg.type is MESSAGE_TYPE_DATA:

            self.logger.info("Sending data to the emoncoms server.")
            self.messenger.send_message(EMONCMS_TITLE, message)
            self.messenger.send_message(UDP_TITLE, message)
            return

        elif msg.type is MESSAGE_TYPE_LOG:

            self.logger.info("Sending log to the splunk server.")
            self.messenger.send_message(SPLUNK_TITLE, message)
            self.messenger.send_message(UDP_TITLE, message)
            return

        # Else nothing to do
        else:
            return

    ## start
    #
    #   This starts the engine thread
    #
    def run(self):

        # We loop until killed
        while self.lock:
            self.link.loop()
        return

    ## stop
    #
    #   This kills the thread
    #
    def stop(self):
        self.lock = False
        return

if __name__ == "__main__":

    from Server_Clients.JSON_Engine.JSON_Engine_Logger_Handler import logger

    logger = logger()
    ipc = json_engine_ipc()


