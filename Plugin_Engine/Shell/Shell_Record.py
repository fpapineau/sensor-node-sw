'''
Created on Nov 10, 2014

@author: fpapinea
'''

#####################################################################
######################## IMPORTS ####################################
#####################################################################

import cmd

__author__ = 'francispapineau'

#####################################################################
######################## CONSTANTS ##################################
#####################################################################

# N / A

#####################################################################
######################## CODE #######################################
#####################################################################

## shell_record_cmd
#
#    The shell process
#
class shell_record_cmd(cmd.Cmd, object):
    
    ## do_record
    #
    #   Record the commands to a specified file
    #
    #   @param arg              - the file name
    #
    def do_record(self, arg):

        # Save future commands to filename:  RECORD rose.cmd
        self.file = open(arg, 'w')
        return
    
    ## help_record
    #
    #     Documentation
    #
    def help_record(self):
        print (""" 
                The record command takes in a filename and records all issued
                commands on the command shell into the provided file.
                
                The <record> command is used as follows:
                    [HomeMon] ->record <filename.tx>
            """)
        return