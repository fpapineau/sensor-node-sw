
#####################################################################
######################## IMPORTS ####################################
#####################################################################

import logging
import threading

from XMPP_Command_Executor import xmpp_command_executor
from XMPP_Command_Parser import xmpp_command_parser


#####################################################################
######################## CONSTANTS ##################################
#####################################################################
# N/A
#####################################################################
######################## CODE #######################################
#####################################################################
## xmpp_job_creator
#
#   This is the command execution job creator, which takes in
#   a command and executes it by spawning a new thread.
#
class xmpp_job_creator(threading.Thread):

    # Class logger
    logger = None

    # Thread alive bool
    alive = True

    # We create a thread pool for our jobs
    thread_pool = None

    # Create a xmpp sender handle
    xmpp_ipc_handle = None
    xmpp_handle = None

    # The run thread handle
    run_thread_handle = None

    ## __init__
    #
    #   This is the default constructor to the class.
    #   It will create our necessary structures to run the thread pool.
    #
    #   @param max_threads              - the max number of threads that can be created
    #
    def __init__(self, xmpp_ipc_handle, xmpp_client_handler):

        # Override the super class
        threading.Thread.__init__(self)

        # Create a logger
        self.logger = logging.getLogger('xmpp_job_creator')

        # Get the xmpp handles
        self.xmpp_ipc_handle = xmpp_ipc_handle
        self.xmpp_handle = xmpp_client_handler

        self.logger.info('Created components for xmpp_job_creator...')

        self.logger.info('Starting Job Creator thread.')
        return

    ## run_creator
    #
    #   This is the running process. It runs indefinitely.
    #
    def run(self):

        # Run while there is no errors
        while self.alive:

            # We read both the queues one after the other and spawn
            # threads from them
            self.read_command_queue()
            self.read_request_queue()

        self.logger.info('Job Creator terminated...')

        # We kill all remaining processes
        self.thread_pool.terminate()

        # We join all threads
        self.thread_pool.join()
        return

    ## kill_running_thread
    #
    #   This kills the job creators thread
    #
    def kill_running_thread(self):

        # Kill the thread
        self.alive = False

        self.logger.info('Job Creator thread going to be terminated...')
        return

    ## create_job
    #
    #   This method spawns a new process for each command received.
    #
    #   @param command                  - the command received
    #
    def create_job(self, command):

        # We start a new thread
        xmpp_command_executor(command, self.xmpp_ipc_handle, self.xmpp_handle).start()
        self.logger.info('Job spawned.')
        return

    ## read_command_queue
    #
    #   This is the read command queue method, we get a command object from the
    #   respective queue.
    #
    def read_command_queue(self):

        # If the queue is not empty
        if not self.xmpp_handle.command_parser.action_command_queue.empty():
            command = self.xmpp_handle.command_parser.action_command_queue.get()
            self.create_job(command)
        return


    ## read_request_queue
    #
    #   This is the read request queue method, we get a command object from the
    #   respective queue.
    #
    def read_request_queue(self):

        # if the queue is not empty
        if not self.xmpp_handle.command_parser.request_command_queue.empty():
            request = self.xmpp_handle.command_parser.action_command_queue.get()
            self.create_job(request)
        return
