__author__ = 'francispapineau'

#####################################################################
######################## IMPORTS ####################################
#####################################################################

from abc import abstractmethod
from Plugin_Engine.PluginTypes.Base_Plugin import base_plugin

#####################################################################
######################## CONSTANTS ##################################
#####################################################################

# N/A

#####################################################################
######################## CODE #######################################
#####################################################################

## reporting_plugin
#
#   This is a generic reporting plugin class that is used for any
#   app that needs to display data onto the command line interface.
#
class reporting_plugin(base_plugin):

    # Auto start flag
    start                               = False

    # The commands available
    required_plugin_commands            = {
                                           "PROCESS"    : ['start',     'stop'],
                                           "DATA"       : [ 'set',      'get']
                                           }

    ## __init__
    #
    # The default Constructor
    #
    def __init__(self):
        base_plugin.__init__(self, self.start)
        self.valid_commands.append(self.required_plugin_commands)
        return
    
    ## get_data
    #
    #   This gets the data from the XML Engine
    #
    @abstractmethod
    def get_data(self):
        raise NotImplemented

    ## display_data
    #
    #   This displays the data from the XML Engine
    #
    @abstractmethod
    def set_data(self):
        raise NotImplemented
