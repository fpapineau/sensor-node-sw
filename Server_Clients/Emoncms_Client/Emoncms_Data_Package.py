
#####################################################################
######################## IMPORTS ####################################
#####################################################################

import time


#####################################################################
######################## CONSTANTS ##################################
#####################################################################
TRUE                = 0
FALSE               = 1

#####################################################################
######################## CODE #######################################
#####################################################################

## emoncms_data_package
#
#   This is the container class for a data package. We use this class
#   as an object that contains the data associated with each sensor. Once
#   we have all the data inputs set within the dictionary, then it is ready
#   to send to the emoncms base.
#
class emoncms_data_package:

    # These are the check flags, to see if we have all
    # the valid data pieces within the container class.
    flags       = {
                "<TMP102>"    : FALSE,
                "<TMP006>"    : FALSE,
                "<SHT21>"     : FALSE,
                "<ADS1015>"   : FALSE,
                "<BMP180>"    : FALSE,
                }

    # The data dict to provide a standard output structure
    data_dict   = {
                "<TMP102>"    : "",
                "<TMP006>"    : "",
                "<SHT21>"     : "",
                "<ADS1015>"   : "",
                "<BMP180>"    : "",
                }

    # The node id
    node        = ""

    ## __init__
    #
    #   This is the default constructor for the class
    #
    def __init__(self):
        pass

    ## append_data
    #
    #   This appends a data piece to the data structure or updates one
    #
    #   @param data                     - the data structure dict
    #
    def append_data(self, data):

        self.data_dict[data['SENSOR']] = self.format_data(data['DATA'])
        self.node = data['NODE']
        self.flags[data['SENSOR']] = TRUE
        return

    ## check_valid
    #
    #   This checks to see if we have a valid packet.
    #
    def check_valid(self):

        status = TRUE

        # Cycle to see all flags
        for item in self.flags.values():
            status |= item
        return status

    ## get_data
    #
    #   This gets the formatted data
    #
    def get_data(self):
        return "?nodeid=" + str(self.node) +\
               "&data=" + str(self.data_dict.values())\
                .replace("'", "").replace(']', '')\
                .replace('[', '').replace(' ', '')

    ## format_data
    #
    #   This is where we format the data pieces.
    #
    def format_data(self, data):
        return data.replace('{', '').replace('}', '').replace(' ', '')

    ## get_node
    #
    #   Gets the node package identifier
    #
    def get_node(self):
        return self.node