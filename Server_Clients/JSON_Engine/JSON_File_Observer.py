
#####################################################################
######################## IMPORTS ####################################
#####################################################################

import logging
import os
import threading
import time

from JSON_Engine_IPC import json_engine_ipc
from Server_Clients.JSON_Engine.JSON_File_Parser import json_file_parser
from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer


#####################################################################
######################## CONSTANTS ##################################
#####################################################################
SUCCESS         = 0
JSON_PATH       = './JSONS/'
PARSED_EXT      = '.parsed'

JSON_TITLE          = "JSON_CREATOR"
EMONCMS_TITLE       = "EMONCMS_SERVER"
SPLUNK_TITLE        = "SPLUNK_SERVER"
LOCALHOST           = ""

EMONCMS_PORT        = 4000
SPLUNK_PORT         = 4001

MESSAGE_TYPE_DATA   = 'D'
MESSAGE_TYPE_LOG    = 'L'
MESSAGE_TYPE_XML    = 'X'

#####################################################################
######################## CODE #######################################
#####################################################################

## message
#
#   A container class
#
class message:
    type        = None
    msg         = None

    def __init__(self):
        return

#####################################################################

## file_observer
#
#   This is the actual event handler class.
#
class file_observer(FileSystemEventHandler):

    # Internal observer handle
    observer                                = None

    # Internal logger handle
    logger                                  = None

    # Internal message container
    msg                                     = message()

    # Internal IPC server
    ipc_server                              = None

    ## __init__
    #
    #   The initial constructor for the class
    #
    # @param observer                       - the observer thread
    #
    def __init__(self, observer):
        self.observer = observer
        self.logger = logging.getLogger("file_observer")
        self.logger.info("Starting the file observer in dir: %s" %JSON_PATH)

        self.logger.info("Starting the ipc server")
        self.ipc_server = json_engine_ipc()
        return

    ## on_modified
    #
    #   The override method for events
    #
    def on_created(self, event):

        data = []

        # We check of the event is a file created
        if not event.is_directory and event.src_path.endswith(".json"):

            self.logger.info("Found a valid file to parse.")

            # We create a new json file parser and pass it the file name
            parser = json_file_parser(event.src_path)

            try:
                data = parser.process_file()
            except Exception:
                self.logger.info("not a valid file to parse.")

            # This is data
            if event.src_path.endswith(".data.json") and data is not []:

                self.logger.info("sending data to the emoncms server")
                # Send the data to the EMONCMS server
                self.msg.type = MESSAGE_TYPE_DATA
                self.msg.msg = data

                # We send the message
                self.ipc_server.send_message(self.msg)

                # Modify the mxg
                self.msg.type = MESSAGE_TYPE_XML
                # Send to the XML Server
                self.ipc_server.send_message(self.msg)

            elif event.src_path.endswith(".log.json") and data is not []:

                self.logger.info("sending a log to the splunk server")
                # Send the data to the EMONCMS server
                self.msg.type = MESSAGE_TYPE_LOG
                self.msg.msg = data

                # We send the message
                self.ipc_server.send_message(self.msg)

                # Modify the mxg
                self.msg.type = MESSAGE_TYPE_XML
                # Send to the XML Server
                self.ipc_server.send_message(self.msg)
        else:
            self.logger.info("not a valid file to parse.")

        # We rename the file parsed
        self.rename_file(event)
        return

    ## rename_file
    #
    # This renames the parsed file so that it is not re-parsed and to allow
    # for some redundancy to be present.
    #
    def rename_file(self, event):

        # We get the file name
        os.rename(event.src_path, event.src_path + PARSED_EXT)
        self.logger.info("renamed the parsed file")
        return

    ##  stop_observing
    #
    #   This unsubscribe the observer.
    #
    def stop_observing(self):

        self.logger.info("stopping the observer.")
        self.observer.unschedule_all()
        return

    ## kill_observer
    #
    #   This kills teh observer thread
    #
    def kill_observer(self):

        self.logger.info("killing the observer thread.")
        self.observer.stop()
        return

#####################################################################

## json_file_observer
#
#   This is a file watchdog that looks within a specified directory fro any file changes.
#   Then the defined handler will take action on the file that has been written or identified
#   as being "changed".
#
class json_file_observer():

    # Internal Observer object
    observer                    = None

    # Internal event handler object
    event_handler               = None

    ## __init__
    #
    #   The default constructor for the class
    #
    def __init__(self, path_to_watch):

        # Create an observer
        self.observer = Observer()

        self.logger = logging.getLogger("json_file_observer")
        self.logger.info("Starting the observer thread.")

        # Add an event handler
        self.event_handler = file_observer(self.observer)

        # Schedule the task
        self.observer.schedule(self.event_handler, path_to_watch, recursive=False)

        # Start the task
        self.observer.start()
        return

if __name__ == "__main__":

    from Server_Clients.JSON_Engine.JSON_Engine_Logger_Handler import logger

    # Create the logger root
    logger = logger()
    observer = json_file_observer("./JSONs")
