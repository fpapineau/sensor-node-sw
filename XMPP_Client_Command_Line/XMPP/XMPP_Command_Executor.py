
#####################################################################
######################## IMPORTS ####################################
#####################################################################

import logging
import threading

from XMPP_Client_Command_Line.XMPP.Command_Handlers.XMPP_Command_Handler_Table import packet_handlers


#####################################################################
######################## CONSTANTS ##################################
#####################################################################
LOOPBACK_CMD                    = "LOOPBACK"

#####################################################################
######################## CODE #######################################
#####################################################################

## xmpp_queue_reader
#
#   This class is an extension of the internal threading class.
#   It provides the execution of the commands
#
class xmpp_command_executor(threading.Thread):

    # The command to execute
    command = None

    # The xmpp_handle
    xmpp_ipc_handle = None

    # The handle
    xmpp_handle = None

    # logger
    logger = logging.getLogger("xmpp_command_executor")

    ## __init__
    #
    #   This is an override of the super class in this case the threading
    #   class. It provides an interface to executing the commands.
    #
    #   @param command              - the command object to execute
    #   @param xmpp_ipc_handle      - the xmpp client handle to access the
    #                                   send method
    #
    def __init__(self, command, xmpp_ipc_handle, xmpp_handle):

        # Override the init from the threading module
        threading.Thread.__init__(self)

        # Get the command to parse
        self.command = command

        # Get the xmpp client handle to send
        self.xmpp_ipc_handle = xmpp_ipc_handle
        self.xmpp_handle = xmpp_handle
        return

    ## run
    #
    #   The overwritten method from the threading class. This is the worker class.
    #
    def run(self):

        # We run the packet handler method
        # We get the function pointer and jump to it with the args.

        if self.command.command not in packet_handlers:
            self.xmpp_handle.send_response("[ERROR]: Not a valid command for callback.")
            self.xmpp_handle.send_response("Commands are as follows: \r %s" %str(packet_handlers.keys()))
            return

        # Setup the context
        handler = packet_handlers[self.command.command]()
        self.logger.info("Setting up the handler.")
        handler.setup(self)


        self.logger.info("Executing a %s callback." %(self.command.command.lower()))
        # Run the app
        if self.command.command == LOOPBACK_CMD:
            self.xmpp_handle.send_response(handler.run(self.command.command))
        else:
            handler.run(self.command)

        # Exit the app
        handler.exit()
        return


