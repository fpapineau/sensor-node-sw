
from XMPP_Client_Command_Line.XMPP.Command_Handlers.XMPP_Command_Handler_Diconnect import disconnecter
from XMPP_Client_Command_Line.XMPP.Command_Handlers.XMPP_Command_Handler_Get import getter
from XMPP_Client_Command_Line.XMPP.Command_Handlers.XMPP_Command_Handler_Loopback import loopback
from XMPP_Client_Command_Line.XMPP.Command_Handlers.XMPP_Command_Handler_Reboot import reboot
from XMPP_Client_Command_Line.XMPP.Command_Handlers.XMPP_Command_Handler_Sutdown import shutdown
from XMPP_Client_Command_Line.XMPP.Command_Handlers.XMPP_Command_Handler_Status import status_checker
from XMPP_Client_Command_Line.XMPP.Command_Handlers.XMPP_Command_Handler_Error import error_handler

# This is a packet handler pointer table, where the packet type is on
# the left hand side and the function pointer is on the left.
packet_handlers = {

    'LOOPBACK'      : loopback,
    
    'BOOT'          : reboot,
    'REBOOT'        : reboot,
    'SHUTDOWN'      : shutdown,

    'DISCONNECT'    : disconnecter,
    
    'STATUS'        : status_checker,
    'GET'           : getter,
    
    'ERROR'         : error_handler

}