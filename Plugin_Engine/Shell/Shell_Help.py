'''
Created on Nov 10, 2014

@author: fpapinea
'''

#####################################################################
######################## IMPORTS ####################################
#####################################################################

import cmd

__author__ = 'francispapineau'

#####################################################################
######################## CONSTANTS ##################################
#####################################################################

# N / A

#####################################################################
######################## CODE #######################################
#####################################################################

## shell_help_cmd
#
# The command history process
#
class shell_help_cmd(cmd.Cmd, object):
    
    ## do_help
    #
    #   This is the help command execution. Either 'help' or '?' works.
    #   Or even ? < command > or help < command > works as well.
    #
    #   @param arg              - the args
    #
    def do_help(self, args):
        ## The only reason to define this method is for the help text in the doc string
        cmd.Cmd.do_help(self, args)
        return
    
    ## help_help
    #
    #     Documentation
    #
    def help_help(self):
        print (""" 
                This command displays the help prompt again and lists
                the command usages.
                
                The <help> command is used as follows:
                    [HomeMon] ->help
                
            """)
        return