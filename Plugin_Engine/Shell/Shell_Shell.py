'''
Created on Nov 10, 2014

@author: fpapinea
'''

#####################################################################
######################## IMPORTS ####################################
#####################################################################

import cmd
import os

__author__ = 'francispapineau'

#####################################################################
######################## CONSTANTS ##################################
#####################################################################

# N / A

#####################################################################
######################## CODE #######################################
#####################################################################

## shell_shell_cmd
#
#    The shell process
#
class shell_shell_cmd(cmd.Cmd, object):
    
    ## do_shell
    #
    #   This is the shell spin out command. The arguments must start
    #   with '!'
    #
    #   @param arg              - the args
    #
    def do_shell(self, args):
        os.system(args)
        return

    ## help_shell
    #
    #     Documentation
    #
    def help_shell(self):
        
        print (""" 
                This command executes an os command within the python
                shell context. 
                
                The <shell / !> command is used as follows:
                    [HomeMon] ->shell <command> or
                    [HomeMon] ->! <command>
            """)
        return
        
        return
