####### XMPP COMMAND CONTAINER #########

import uuid


## xmpp_command
#
#   This class is a container class for a command object, received
#   from the XMPP connection.
#
class xmpp_command(object):

    # Delay before command is executed
    command_delay = None

    # The command timestamp (received)
    command_timestamp = None

    # This is a cancelling mechanism flag
    command_alive = False

    # The command arguments
    command_args = []

    # Command id
    command_id = None

    # A tracking uuid
    command_uuid = None

    # The command to execute
    command = None


    ## __init__
    #
    #   This is the class constructor.
    #
    #   @param delay            - the command delay
    #   @param timestamp        - the command received timestamp
    #   @param args             - the arguments passed
    #   @param command          - the command to execute
    #
    def __init__(self, delay, timestamp, args, command, command_alive=False):

        # Set the values
        self.command_uuid = uuid.uuid1()
        self.command_delay = delay
        self.command_args = args
        self.command_timestamp = timestamp
        self.command = command
        self.command_alive = True
        return

    ## set_command_id
    #
    #   This sets the command id of the command in the queue.
    #   We use this for tracking purposes only.
    #
    #   @param command_id       - the id of the command
    #
    def set_command_id(self, command_id):

        self.command_id = command_id
        return

    ## __delete
    #
    #   This method deletes an instance passed.
    #
    #   @param instance         - the instance to delete
    #
    def __delete__(self, instance):
        del instance
        return

