'''
Created on Nov 10, 2014

@author: fpapinea
'''

#####################################################################
######################## IMPORTS ####################################
#####################################################################

import cmd

__author__ = 'francispapineau'

#####################################################################
######################## CONSTANTS ##################################
#####################################################################

# N / A

#####################################################################
######################## CODE #######################################
#####################################################################

## shell_history_cmd
#
# The command history process
#
class shell_execute_cmd(cmd.Cmd, object):
    
    ## do_execute
    #
    #   This executes a function
    #
    #   @param args             - the parameters
    #                                   - method {parameters}
    #
    def do_execute(self, arg):

        if(len(arg) < 1):
            return

        # split command and plugin
        args            = arg.split(" ")
        plugin          = args[0]
        command         = args[1]
        args            = ''
        
        # split command and arg
        command_args    = command.split(":")
        if(len(command_args) > 1):
            command     = arg[0]
            args        = arg[1]

        plugin = self.manager.getPluginByName(plugin)
        function = getattr(plugin.plugin_object, command)
        if function is not None:
            # check to see args
            if args is not '':
                try :
                    function(args)
                except Exception:
                    print("Error")
                return
            else:
                try :
                    function()
                except Exception:
                    print("Error")
                return

        print ("Command <%s> not found." %command)
        return
    
    ## help_execute
    #
    #     Documentation
    #
    def help_execute(self):
        print (""" 
                The execute command takes in plugin name and a command to execute
                a plugin task given that the plugin command is in fact valid.
                
                The <execute> command is used as follows:
                    [HomeMon] ->execute <plugin> <command [: arg]>
            """)
        return