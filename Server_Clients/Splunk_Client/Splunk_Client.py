

#####################################################################
######################## IMPORTS ####################################
#####################################################################

import logging
import socket
import threading

from Splunk_Client_Logger_Handler import logger
from Splunk_IPC import splunk_client_ipc
import simplejson


#####################################################################
######################## CONSTANTS ##################################
#####################################################################
JSON_TITLE          = "JSON_CREATOR"
EMONCMS_TITLE       = "EMONCMS_SERVER"
SPLUNK_TITLE        = "SPLUNK_SERVER"
LOCALHOST           = ""

EMONCMS_PORT        = 4000
SPLUNK_PORT         = 4001

MESSAGE_TYPE_DATA   = "D"
MESSAGE_TYPE_LOG    = "L"

SPLUNK_HOST         = "10.0.1.2"
SPLUNK_SERVER_PORT  = 9999
SPLUNK_USERNAME     = "admin"
SPLUNK_PASSWORD     = "admin"

#####################################################################
######################## CODE #######################################
#####################################################################

## splunk_server_interface
#
#   This is the class handler for the splunk server.
#
class splunk_server_interface(threading.Thread):

    # Internal lock
    lock                        = True

    # Internal IPC Server
    ipc_server                  = None

    # Internal logger
    logger                      = None

    # The service endpoint
    socket                      = None

    ## __init__
    #
    #   This is the default constructor for the class.
    #
    def __init__(self, host, port):

        # Override the thread class
        threading.Thread.__init__(self)

        # Set the logger
        self.logger = logging.getLogger("splunk_server_interface")
        self.logger.info("Starting the Splunk interface.")
        self.logger.info("connecting to the server: %s:%d." %(host, int(port)))

        # Connect to the server
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((host, int(port)))
        self.logger.info("Connected.")
        return

    ## start
    #
    #   Start the engine thread
    #
    def run(self):

        # While run lock is true
        while self.lock:

            # We check the JSON parser.
            # We get the dicts from the internal queue
            while not self.ipc_server.queue.empty():

                # Get a piece of the puzzle
                data = self.ipc_server.queue.get()
                self.socket.sendall(simplejson.dumps(data.data))
                self.logger.info("sent packet to splunk server: %s" %simplejson.dumps(data.data).replace('\\', ''))
        return

    ## kill
    #
    #   Kill the thread
    #
    def kill(self):
        self.lock = False
        return

    ## connect_ipc
    #
    #   Start ipc
    #
    def connect_ipc(self):

        self.logger.info("Starting an IPC client")
        # We create an ipc server
        self.ipc_server = splunk_client_ipc()
        self.ipc_server.start()
        return

    ## disconnect_ipc
    #
    #   Start ipc
    #
    def disconnect_ipc(self):

        self.logger.info("Stopping an IPC client")
        self.ipc_server.kill()
        return