
#####################################################################
######################## IMPORTS ####################################
#####################################################################

from XMPP_Command_Handler_Base import xmpp_command_handler


#####################################################################
######################## CONSTANTS ##################################
#####################################################################
# N/A
#####################################################################
######################## CODE #######################################
#####################################################################
class reboot(xmpp_command_handler):

    ## __init__
    #
    #   This is the constructor for object access
    #
    def __init__(self):
        pass

    ## setup
    #
    #   The setup method to set the context of the handler.
    #   Nothing to setup in this task
    #
    def setup(self, executer):

        # Set the execution pointer
        self.execution = executer
        return

    ## stop
    #
    #   This method stops the work of the handler.
    #   Nothing to be done in this task
    #
    def stop(self):
        pass

    ## destroy
    #
    #   This exits the handler.
    #
    def exit(self):
        pass
