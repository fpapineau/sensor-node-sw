
#####################################################################
######################## IMPORTS ####################################
#####################################################################

import json
import logging
import os
import threading
import time

import Queue


#####################################################################
######################## CONSTANTS ##################################
#####################################################################
JSON_PATH                   = './JSONS/'
DATA_FILE_NAME              = 'DATA_'
LOG_FILE_NAME               = 'LOG_'
DATA_EXT                    = '.data.json'
LOG_EXT                     = '.log.json'
DATA                        = 1
LOG                         = 2

#####################################################################
######################## CODE #######################################
#####################################################################

## json_status_updater
#
#   This is the class handler that handles the creation of both
#   the log files and the data files.
#
class json_status_updater(threading.Thread):

    # Updates queues
    # The data queue
    data_queue                  = Queue.Queue()

    # The log queue
    log_queue                   = Queue.Queue()

    # Thread status
    alive                       = True

    # logger
    logger                      = None

    ## __init__
    #
    #   The default constructor
    #
    def __init__(self):

        # Override the thread
        threading.Thread.__init__(self)

        # Get a logger
        self.logger = logging.getLogger("json_status_updater")
        self.logger.info("starting the status updater.")
        self.start()
        return

    ## run
    #
    #   The worker method
    #
    def run(self):

        # Stay here for a while
        while self.alive:

            # Check to see if we got new data
            if not self.data_queue.empty():
                self.logger.info("creating a data log")
                self.create_data_log()

            # Check to see if we got new logs
            if not self.log_queue.empty():
                self.logger.info("creating a system log")
                self.create_system_log()

            # Sleep for a bit
            time.sleep(3)
        return

    ## stop
    #
    #   Stops the thread from working
    #
    def stop(self):
        self.alive = False
        return

    ## create_data_log
    #
    #   This gets any logs in the internal data queue and creates
    #   a json file with extension .data.json
    #
    def create_data_log(self):

        data = []

        # We get a data piece
        while not self.data_queue.empty():
            data.append(self.data_queue.get())

        # Open a file
        file = open(JSON_PATH + self.get_file_name(DATA), 'w')

        # Write the data
        for item in data:
            file.write(json.dumps(str(item)))

        # Close the file
        file.close()
        return

    ## create_system_log
    #
    #   This gets any logs in the internal log queue and creates
    #   a json file with extension .log.json
    #
    def create_system_log(self):

        logs = []

        # We get a data piece
        while not self.log_queue.empty():
            logs.append(self.log_queue.get())

        # Open a file
        file = open(JSON_PATH + self.get_file_name(LOG) , 'w')

        # Write the data
        for item in logs:

            string = json.dumps(item)
            file.write(string)

        # Close the file
        file.close()
        return

    ## get_file_name
    #
    #   This gets a time name based on the passed log type
    #
    @staticmethod
    def get_file_name(type):

        file_name = ''

        # Data
        if type == DATA:
            file_name += DATA_FILE_NAME + time.strftime("%Y-%m-%d_%H:%M:%S", time.gmtime()) + DATA_EXT

        # Log
        elif type == LOG:
            file_name += LOG_FILE_NAME + time.strftime("%Y-%m-%d_%H:%M:%S", time.gmtime()) + LOG_EXT
        return file_name



