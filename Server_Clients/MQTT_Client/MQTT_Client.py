
#####################################################################
######################## IMPORTS ####################################
#####################################################################

import threading
import logging
import time

from MQTT_Client_Logger_Handler import logger
from MQTT_IPC_Server import communication_server
from MQTT_Interface import mqtt_interface


#####################################################################
######################## CONSTANTS ##################################
#####################################################################

SUCCESS = 0

#####################################################################
######################## CODE #######################################
#####################################################################

## mqtt_client
#
#   This is the MQTT client which will connect to the MQTT server on the
#   Raspberry Pi. It will publish requests and receive subscriptions.
#
#   This class uses the SocketServer class to implement a socket server,
#   as the primary method of passing requests to the MQTT server. In a
#   sense this class is a proxy to the MQTT server.
#
class mqtt_client(threading.Thread):

    # The mqtt client
    mqtt_client_handle  = None

    # The communication server
    ipc_server_handle   = None

    # Server settings
    server_settings     = dict()

    # MQTT client settings
    client_settings     = dict()

    # The killing bool
    alive               = True

    # Logger
    logger              = None

    ## __init__
    #
    #   This is the default constructor
    #
    def __init__(self, ident, port, host):
        threading.Thread.__init__(self)

        # Set a logger
        self.logger = logging.getLogger('mqtt_client')

        # Set the client settings dict
        self.client_settings["ID"] = ident
        self.client_settings["PORT"] = int(port)
        self.client_settings["HOST"] = host

        # Spawn the threads
        try:
            self.spawn_mqtt_thread()
        except Exception:
            raise Exception
            return

        try:
            self.spawn_ipc_socketserver_thread()
        except Exception:
            raise Exception
            return
        return

    ## spawn_ipc_socketserver_thread
    #
    #   This starts the ipc socket server thread.
    #
    def spawn_ipc_socketserver_thread(self):

        # Start the IPC
        self.ipc_server_handle = communication_server(self.mqtt_client_handle)
        self.ipc_server_handle.start()
        return

    ## spawn_mqtt_thread
    #
    #   This starts the mqtt server thread.
    #
    def spawn_mqtt_thread(self):

        # Start the client
        try:
            self.mqtt_client_handle = mqtt_interface(self.client_settings)
        except Exception:
            self.kill()
            raise Exception
        return

    ## run
    #
    #   This is the main worker method.
    #
    def run(self):

        try:

            while self.alive:
                time.sleep(1)

        except KeyboardInterrupt:
            self.mqtt_client_handle.stop_engine()
            self.ipc_server_handle.stop_engine()
            return
        return

    ## kill
    #
    #   Kills the thread
    #
    def kill(self):
        self.logger.info("Killing thread")
        self.alive = False
        return

