
"""
    This is the system startup script. It is used to boot the entire system.
    Follow the appropriate argument list in order to select different boot
    types, such as debugging.
"""

import argparse
import ConfigParser
import sys

from XMPP_Client_Command_Line.XMPP.XMPP_Client import xmpp_client, xmpp_connection_settings
from Server_Clients.MQTT_Client.MQTT_Client import mqtt_client
from Server_Clients.JSON_Engine.JSON_File_Observer import json_file_observer, file_observer
from Server_Clients.JSON_Engine.JSON_Engine_Status_Updater import json_status_updater
from Server_Clients.Emoncms_Client.Emoncms_Client import emoncms_server_interface
from Server_Clients.Splunk_Client.Splunk_Client import splunk_server_interface
from Plugin_Engine.Plugin_Manager import plugin_manager
from Server_Clients.UDP_Server.UDP_Server import udp_server

CONFIG_SECTIONS     = ['XMPP_CLIENT_CONFIGS', 'MQTT_CLIENT_CONFIGS',
                        'JSON_ENGINE_CONFIGS', 'EMONCMS_CLIENT_CONFIGS',
                        'SPLUNK_CLIENT_CONFIGS']

SUCCESS             = 5
JSON_ROOT_DIR       = './JSONS/'

### We can either select the file directly or enter the args manually
parser = argparse.ArgumentParser(description="Home monitoring bridge system application.")

### XMPP
parser.add_argument('-u', '--xmpp_user', help='Your username and resource to log into the xmpp server')
parser.add_argument('-p', '--xmpp_password', help='Your password to log into the xmpp server')

### MQTT
parser.add_argument('-i', '--mqtt_id', help='Your MQTT sever jabber id.')
parser.add_argument('-m', '--mqtt_port', help='Your MQTT server port.')
parser.add_argument('-M', '--mqtt_host', help='Your MQTT server host address.')

### JSON
parser.add_argument('-d', '--json_directory', help='The directory that the JSON engine will scan.')

### EMONCMS
parser.add_argument('-P', '--emoncms_protocol', choices=['http://', 'https://'],
                    help='The EMONCMS server protocol for http posts, either [http] or [https]')
parser.add_argument('-g', '--emoncms_domain', help='The address of the EMONCMS server.')
parser.add_argument('-r', '--emoncms_resource', help='The EMONCMS server resource name.')
parser.add_argument('-a', '--emoncms_key', help='The API key for read/write ability on the EMONCMS server.')

### SPLUNK
parser.add_argument('-s', '--splunk_port', help='The Splunk server port.')
parser.add_argument('-S', '--splunk_host', help='The Splunk server host name.')

### UPD Server
parser.add_argument('-w', '--udp_server_address', help='UDP Server address')
parser.add_argument('-W', '--udp_server_port', help='UDP Server port')

### OTHERS
#   verbose
parser.add_argument('-v', '--verbose', action='store_true', help='The verbosity flag.')

#   file
parser.add_argument('-f', '--configs', help='The location of the configuration file.')

#   plugin test
parser.add_argument('-l', '--plugin_test', action='store_true', help='This tests the plugin.')

# parse the args on the stack
arguments = parser.parse_args()

####
# --------------------------------------------------------------------------------------------
####

## load_plugin_engine
#
# Loads the plugin engine
#
def load_plugin_engine():

    # Start Plugin Engine
    manager = plugin_manager()
    return

####
# --------------------------------------------------------------------------------------------
####

if __name__ == "__main__":

    import logging
    import os

    from HomeMon_Logger_Handler import logger

    ptr = dict()

    _logger = logger()
    log = logging.getLogger('Main')

    # Check if there is a plugin test toggle
    if arguments.plugin_test:
        load_plugin_engine()
        exit(0)


####
# --------------------------------------------------------------------------------------------
####

    log.info("Checking the file switch")
    if arguments.configs is not None:

        # Create a config parser
        configs = ConfigParser.ConfigParser()

        log.info("Reading the configs")
        # Read the configs
        configs.read(arguments.configs)

        # Check for valid sections and values
        if configs.sections() is not []:

            count = 0
            for item in configs.sections():
                if item in CONFIG_SECTIONS:
                    count += 1

            # File corrupt
            if count != SUCCESS:
                log.error("Configuration file corrupt.")
                exit(1)

            # Good file
            else:

                log.info("Configuration file read successfully.")
                log.info("Getting the values.")
                # Assign the pointers
                ptr["XMPP_USER"] = configs.get("XMPP_CLIENT_CONFIGS", "xmpp_username_handle")
                ptr["XMPP_PASSWORD"] = configs.get("XMPP_CLIENT_CONFIGS", "xmpp_server_password")

                ptr["MQTT_ID"] = configs.get("MQTT_CLIENT_CONFIGS", "mqtt_id")
                ptr["MQTT_PORT"] = configs.get("MQTT_CLIENT_CONFIGS", "mqtt_port")
                ptr["MQTT_HOST"] = configs.get("MQTT_CLIENT_CONFIGS", "mqtt_host")

                ptr["JSON_DIR"] = configs.get("JSON_ENGINE_CONFIGS", "json_dump_location")

                ptr["EMON_PROTOCOL"] = configs.get("EMONCMS_CLIENT_CONFIGS", "emoncms_protocol")
                ptr["EMON_DOMAIN"] = configs.get("EMONCMS_CLIENT_CONFIGS", "emoncms_domain")
                ptr["EMON_RES"] = configs.get("EMONCMS_CLIENT_CONFIGS", "emoncms_path")
                ptr["EMON_KEY"] = configs.get("EMONCMS_CLIENT_CONFIGS", "emoncms_api_key")

                ptr["SPLUNK_PORT"] = configs.get("SPLUNK_CLIENT_CONFIGS", "splunk_port")
                ptr["SPLUNK_HOST"] = configs.get("SPLUNK_CLIENT_CONFIGS", "splunk_host")

                ptr["UDP_PORT"] = configs.get("UDP_RPC_SERVER_CONFIGS", "udp_port")
                ptr["UDP_ADDRESS"] = configs.get("UDP_RPC_SERVER_CONFIGS", "udp_server")

####
# --------------------------------------------------------------------------------------------
####

    # Get the parameters
    else:
        log.info("No file inputted, getting input args from command line.")

        # Assign the pointers
        ptr["XMPP_USER"] = arguments.xmpp_user
        ptr["XMPP_PASSWORD"] = arguments.xmpp_password
        ptr["MQTT_ID"] = arguments.mqtt_id
        ptr["MQTT_PORT"] = arguments.mqtt_port
        ptr["MQTT_HOST"] = arguments.mqtt_host
        ptr["JSON_DIR"] = arguments.json_directory
        ptr["EMON_PROTOCOL"] = arguments.emoncms_protocol
        ptr["EMON_DOMAIN"] = arguments.emoncms_domain
        ptr["EMON_RES"] = arguments.emoncms_resource
        ptr["EMON_KEY"] = arguments.emoncms_key
        ptr["SPLUNK_PORT"] = arguments.splunk_port
        ptr["SPLUNK_HOST"] = arguments.splunk_host
        ptr["UDP_PORT"] = arguments.udp_server_port
        ptr["UDP_ADDRESS"] = arguments.udp_server_address


        # check validity
        if None in ptr.values():
            log.error("Argument problem.")
            parser.print_help()
            exit(1)

####
# --------------------------------------------------------------------------------------------
####

    # Execute the rm for files
    log.info("Removing the previous JSON files if there are any.")
    os.system("rm ./JSONS/*.json")
    os.system("rm ./JSONS/*.parsed")

    # We start the engines
    log.info("Starting system engines.")

####
# --------------------------------------------------------------------------------------------
####

    mqtt_engine = None

    # MQTT Client
    string = "Booting MQTT client engine ".ljust(75, '.')
    try:
        mqtt_engine = mqtt_client(ptr["MQTT_ID"], ptr["MQTT_PORT"], ptr["MQTT_HOST"])

    except Exception:

        string += ' [ Failed ]'
        log.error(string)
        log.info("Exiting main context.")
        sys.exit(1)

    mqtt_engine.setDaemon(True)
    mqtt_engine.start()

    string += ' [ Ok ]'
    log.info(string)

####
# --------------------------------------------------------------------------------------------
####

    # We start a udp server for the rpc application request serving
    # UDP Server
    string = "Booting UDP Request Server engine ".ljust(75, '.')
    try:
        udp_server = udp_server()

    except Exception:
        string += ' [ Failed ]'
        log.error(string)
        log.info("Exiting main context.")
        sys.exit(1)

    string += ' [ Ok ]'
    log.info(string)


####
# --------------------------------------------------------------------------------------------
####

    # JSON Client
    string = "Booting JSON engine ".ljust(75, '.')
    try:
        json_engine = json_file_observer('./JSONS/')
        json_updater = json_status_updater()

    except Exception:
        string += ' [ Failed ]'
        log.error(string)
        log.info("Exiting main context.")
        sys.exit(1)

    string += ' [ Ok ]'
    log.info(string)


####
# --------------------------------------------------------------------------------------------
####

    # EMONCMS Client
    string = "Booting EMONCMS client engine ".ljust(75, '.')
    try:
        emoncms_engine = emoncms_server_interface(ptr["EMON_PROTOCOL"],
                                                  ptr["EMON_DOMAIN"],
                                                  ptr["EMON_RES"],
                                                  ptr["EMON_KEY"])
        emoncms_engine.setDaemon(True)
        emoncms_engine.start()

    except Exception:
        string += ' [ Failed ]'
        log.error(string)
        log.info("Exiting main context.")
        sys.exit(1)

    string += ' [ Ok ]'
    log.info(string)

####
# --------------------------------------------------------------------------------------------
####

    # Splunk Client
    string = "Booting Splunk client engine ".ljust(75, '.')
    # try:
    splunk_engine = splunk_server_interface(ptr["SPLUNK_HOST"],
                                            ptr["SPLUNK_PORT"])
    splunk_engine.setDaemon(True)
    splunk_engine.start()

####
# --------------------------------------------------------------------------------------------
####

    # XMPP Client
    string =  "Booting XMPP client engine ".ljust(75, '.')
    try:

        settings = xmpp_connection_settings(ptr["XMPP_USER"], ptr["XMPP_PASSWORD"])
        xmpp_engine = xmpp_client(settings)

    except Exception:
        string += ' [ Failed ]'
        log.error(string)
        log.info("Exiting main context.")
        sys.exit(1)

    string += ' [ Ok ]'
    log.info(string)

####
# --------------------------------------------------------------------------------------------
####

    # Instantiates the Message Bus for the plugin
    # - This is used to communicate from the plugins to the main app

    # This is the work loop. We use this to run the plugin applications.

    ## Load the plugins
    ## Print the plugins that are loaded
    ## -- Run the app
    load_plugin_engine()
