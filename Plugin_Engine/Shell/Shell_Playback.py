'''
Created on Nov 10, 2014

@author: fpapinea
'''

#####################################################################
######################## IMPORTS ####################################
#####################################################################

import cmd

__author__ = 'francispapineau'

#####################################################################
######################## CONSTANTS ##################################
#####################################################################

# N / A

#####################################################################
######################## CODE #######################################
#####################################################################

## shell_playback_cmd
#
#    The shell process
#
class shell_playback_cmd(cmd.Cmd, object):
    
    
    ## do_playback
    #
    #   Playback the commands from a specified file
    #
    #   @param arg              - the file name
    #
    def do_playback(self, arg):

        # Playback commands from a file:  PLAYBACK rose.cmd
        self.close()
        with open(arg) as f:
            self.cmdqueue.extend(f.read().splitlines())
        return
    
    ## help_playback
    #
    #     Documentation
    #
    def help_playback(self):
        print (""" 
                The playback command takes in a filename and plays all recorded all issued
                commands on the command shell from the provided file.
                
                The <playback> command is used as follows:
                    [HomeMon] ->playback <filename.tx>
            """)
        return