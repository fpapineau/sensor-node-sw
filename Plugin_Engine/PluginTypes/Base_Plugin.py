__author__ = 'francispapineau'

#####################################################################
######################## IMPORTS ####################################
#####################################################################

from abc import abstractmethod, ABCMeta
from yapsy.IPlugin import IPlugin

import ConfigParser
import logging
import uuid

#####################################################################
######################## CONSTANTS ##################################
#####################################################################

CONFIG_FILE_FOLDER = './Plugins/'

#####################################################################
######################## CODE #######################################
#####################################################################
## calculation_plugin
#
#   This is a generic calculation plugin class that is used for any
#   app that needs to calculate data onto the command line interface.
#
class base_plugin(IPlugin):

    # The commands available
    valid_commands      = []
    
    # Auto start flag
    auto_start          = False

    # Class signature
    signature           = ""
    
    # Configuration Parser
    config_parser       = ConfigParser.ConfigParser()
    
    # Config file to parse
    config_file         = ""
    
    # Create a logger object
    logger_object   = logging.getLogger("test_plugin")

    # Required commands
    required_plugin_commands = None

    # Configs
    configs             = None

    ## __init__
    #
    #   The constructor
    #
    def __init__(self, auto_start):
        IPlugin.__init__(self)
        
        # Set the auto start bool
        self.auto_start = auto_start

        # Set the config reader
        self.configs = ConfigParser.ConfigParser()
        return

    ## execute
    #
    #   the execution method
    #
    @abstractmethod
    def execute(self, command):
        raise NotImplemented

    ## is_auto_start
    #
    #    Check is the plugin is an autostart plugin
    #
    def is_auto_start(self):
        return self.auto_start
    
    ## set_config_location
    #
    #    Sets the location of the config file
    #
    def set_config_location(self, location):
        self.config_file = CONFIG_FILE_FOLDER + location 
        self.logger_object.info("Loaded the base plugin object")
        return
    
    ## get_valid_commands
    #
    #   This gets all commands that are valid within
    #   this plugin
    #
    def get_valid_commands(self):
        return self.required_plugin_commands
    
    ## print_signature
    #
    #   Prints the class signature
    #
    def get_signature(self):
        # Set the signature
        self.signature = "%s -- " %self.__class__.__name__ + str(uuid.uuid4())
        return self.signature

    ## read_configs
    #
    #   We read the configs
    #
    def read_configs(self):

        # We get the configs
        self.logger_object.info("Reading the configs")

        # Read the configs
        self.configs.read(self.config_file)

        # Check for valid sections and values
        if self.configs.sections() > 0 :
            return self.configs
        else:
            return None

    ## stop
    #
    #   shuts the client down
    #
    @abstractmethod
    def stop(self):
        raise NotImplemented

    ## start
    #
    #   starts the client if connected
    #
    def run(self):
        
        # We start by reading the config file
        # We get the attributes internally
        self.logger_object.info("Reading the plugin config file.")
        self.logger_object.info("Reading ... %s" %self.config_file)
        self.config_parser.read(self.config_file)
        self.logger_object.info("Configurations are read. They are internally stored.")
        
        # We call the run method
        self.run()
        return
    
    ## setup
    #
    #    The worker method.
    #
    @abstractmethod
    def setup(self):
        raise NotImplemented
    
    
        