

#####################################################################
######################## IMPORTS ####################################
#####################################################################

import logging

from Splunk_Client import splunk_server_interface
from Splunk_Client_Logger_Handler import logger


#####################################################################
######################## CONSTANTS ##################################
#####################################################################
# N/A
#####################################################################
######################## CODE #######################################
#####################################################################
# Create a logger
logger = logger()
log = logging.getLogger("Splunk Service")

# Create a client
client = splunk_server_interface()

log.info("Starting worker thread")
client.setDaemon(True)
client.start()

log.info("Started Splunk service")
# run until dead