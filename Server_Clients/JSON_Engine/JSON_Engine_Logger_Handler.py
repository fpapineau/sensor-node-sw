
#####################################################################
######################## IMPORTS ####################################
#####################################################################

import logging
import logging.config
import time


#####################################################################
######################## CONSTANTS ##################################
#####################################################################
# N/A
#####################################################################
######################## CODE #######################################
#####################################################################
## raspbee_logger
#
#   This class in the application logger class.
#   It provides the base class for loggers within the
#   application.
#
#   Status = DONE
#
class logger:

    # @var logger - The logger object.
    logger = None

    # @var logger_level - The logger level
    logger_level = logging.INFO

    # @var logger_formatter - The logger formatter
    logger_formatter = None

    # @var logger_console - Console logger
    logger_console = None

    #Log file name
    LOG_FILE_NAME = './Log/JSON-Engine-%s.log'

    ## __init__
    #
    #   This is the default constructor for the class.
    #   It creates the root logger and the log file that also goes along
    #   with it.
    #
    def __init__(self):

        # We set the file name
        filename = self.LOG_FILE_NAME %time.strftime('%d-%m-%y')

        # set up logging to file - see previous section for more details
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s %(name)-20s %(levelname)-8s %(message)s',
                            datefmt='%m-%d %H:%M',
                            filename=filename,
                            filemode='a')

        # We create the root logger for console
        self.logger_console = logging.StreamHandler()
        self.logger_console.setLevel(self.logger_level)

        # Create a formatter
        self.logger_formatter = logging.Formatter('%(asctime)s %(name)-20s: %(levelname)-8s %(message)s')

        # We set the formatters
        self.logger_console.setFormatter(self.logger_formatter)

        # We set the root handlers
        logging.getLogger('').addHandler(self.logger_console)

        return

    ## create_logger
    #
    #   This creates a spin off logger with the given class name.
    #
    #   @param class_name - name of the logger
    #
    def create_logger(self, class_name):
        return logging.getLogger(class_name)

    ## delete_logger
    #
    #   This method deletes a passed instance of logger.
    #
    #   @param logger - logger to delete
    #
    def delete_logger(self, logger):
        del logger
        return


## main
#
#   The main
#
if __name__ == '__main__':

    test_logger = logger()
    class_logger = test_logger.create_logger('test')
