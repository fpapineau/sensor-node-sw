
#####################################################################
######################## IMPORTS ####################################
#####################################################################

from XMPP_Client_Command_Line.XMPP.XMPP_Command import xmpp_command
from XMPP_Command_Handler_Base import xmpp_command_handler


#####################################################################
######################## CONSTANTS ##################################
#####################################################################
# N/A
#####################################################################
######################## CODE #######################################
#####################################################################
## loopback
#
#   This is a purely testing class used to see if the messages pass
#   from the user to the server and back.
#
class loopback(xmpp_command_handler):

    # We create an empty packet to send off
    packet = ''

    ## __init__
    #
    #   This is the constructor for object access
    #
    def __init__(self):
        pass

    ## setup
    #
    #   The setup method to set the context of the handler.
    #   Nothing to setup in this task
    #
    def setup(self, executer):

        # Set the execution pointer
        self.execution = executer

        # We assign a header to the packet.
        self.packet = "[LOOPBACK]: ARGS -> %s"

    ## run
    #
    #   The run method that actually runs the method within the
    #   context setup above.
    #
    def run(self, args):

        # We send the command send with our loopback packet header
        return self.packet %args

    ## stop
    #
    #   This method stops the work of the handler.
    #   Nothing to be done in this task
    #
    def stop(self):
        pass

    ## destroy
    #
    #   This exits the handler.
    #
    def exit(self):
        pass
