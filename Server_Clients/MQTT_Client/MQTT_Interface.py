
#####################################################################
######################## IMPORTS ####################################
#####################################################################

import logging

from MQTT_Message_Processor import mqtt_message_processor
import Queue
from Server_Clients.JSON_Engine.JSON_Engine_Status_Updater import json_status_updater
from mosquitto import Mosquitto, MOSQ_ERR_SUCCESS


#####################################################################
######################## CONSTANTS ##################################
#####################################################################
SUCCESS = 0

#####################################################################
######################## CODE #######################################
#####################################################################

## mqtt_interface
#
#   This class provides an interface to the mqtt server through a client.
#
class mqtt_interface(Mosquitto):

    # The mqtt connection handle
    client_handle           = None

    # The mqtt client settings
    client_settings         = None

    # The class logger
    logger                  = None

    # Generic Topic         - Monitor
    home_topic              = "home/#"

    # Generic Topic         - Control / Sensors
    sens_control_topic      = "control/#"
    global_control_topic    = "glb_cmd"

    # The internal topics
    topic_names             = []

    # XML Engine pointer
    json_engine             = json_status_updater()

    # Command response
    command_response        = False
    command_queue           = Queue.Queue()

    # Status
    status                  = None

    ## __init__
    #
    #   This is the default constructor for the class
    #
    #   @param client_settings              - the client settings in a dictionary
    #
    def __init__(self, client_settings):

        # Create a logger object
        self.logger = logging.getLogger('mqtt_interface')

        # Create a client handle
        self.client_settings = client_settings

        # Override the super class
        Mosquitto.__init__(self, self.client_settings['ID'], clean_session=True)
        self.logger.info('Created a mosquitto mqtt client')

        # Connect to the mqtt server

        self.status = self.connect(self.client_settings['HOST'],
                    self.client_settings['PORT'])

        # Error occurred
        if self.status is not MOSQ_ERR_SUCCESS:
            self.logger.error('Not connected to the mqtt server.')
            return

        self.logger.info('Connected to the mqtt server.')
        self.logger.info('Starting mqtt client engine')

        # Set the callbacks
        self.on_connect         = self.on_connect_t
        self.on_disconnect      = self.on_disconnect_t
        self.on_subscribe       = self.on_subscribe_t
        self.on_publish         = self.on_publish_t
        self.on_message         = self.on_message_t
        self.on_unsubscribe     = self.on_unsubscribe_t

        # Run the mqtt engine
        self.start_engine()

        # Subscribe to the topics in general
        self.logger.info("Subscribing to: %s", self.home_topic)
        self.subscribe(self.home_topic,         0)
        self.logger.info("Subscribing to: %s", self.sens_control_topic)
        self.subscribe(self.sens_control_topic, 0)
        return

    ## kill_thread
    #
    #   This kills the mqtt client thread
    #
    def kill_thread(self):

        self.logger.info('Killing thread forcefully')
        self.loop_stop(True)
        return

    ## start_engine
    #
    #   This starts the mqtt engine thread
    #
    def start_engine(self):

        self.logger.info('Starting engine thread')
        self.loop_start()
        return

    ## stop_engine
    #
    #   This stops the engine softly
    #
    def stop_engine(self):

        self.logger.info('Killing thread softly')
        self.loop_stop(False)
        return


    ## process_message
    #
    #   Processes the received data from a callback and starts a thread context,
    #   for that specific callback action
    #
    #   @param msg                          - the message structure
    #
    def process_message(self, msg):

        # Calling the process to process the callback message
        self.logger.info("Starting a callback thread for topic: [ topic: %s ] -> data: %s" %(msg.topic, msg.payload))
        mqtt_message_processor(msg, self.json_engine)
        return

    #####
    # The callbacks
    #####

    ## on_connect
    #
    #   This method is fired when we connect to the mqtt server.
    #
    #   @param userdata                     - the passed userdata
    #   @param rc                           - status code
    #
    @staticmethod
    def on_connect_t(obj, userdata, rc):

        if rc == MOSQ_ERR_SUCCESS:
            obj.logger.info('Successfully connected.')
        else:
            obj.logger.error('Connection error.')
        return

    ## on_disconnect
    #
    #   This method is fired when we disconnect for the mqtt server.
    #
    #   @param userdata                     - the passed userdata
    #   @param rc                           - status code
    #
    @staticmethod
    def on_disconnect_t(obj, userdata, rc):

        if rc == MOSQ_ERR_SUCCESS:
            obj.logger.info('Successfully disconnected.')
        else:
            obj.logger.error('Connection error.')
        return

    ## on_message
    #
    #   This method is fired when we receive a message.
    #
    #   @param userdata                     - the passed userdata
    #   @param msg                          - the returned message
    #
    @staticmethod
    def on_message_t(obj, userdata, msg):

        obj.logger.info('Message received on topic ' + msg.topic
                         + ' with QoS '+ str(msg.qos)
                         + ' and payload ' + str(msg.payload))

        # Add the topic to the topic names if not there.
        if msg.topic not in obj.topic_names:
            obj.topic_names.append(msg.topic)

        # Call the processor thread
        obj.process_message(msg)

        # If its a command response
        if obj.command_response:

            # put it in the commadn queue
            obj.command_queue.put(msg)

        return

    ## on_publish
    #
    #   This method is fired when we publish a message.
    #
    #   @param userdata                     - the passed userdata
    #   @param mid                          - the message id
    #
    @staticmethod
    def on_publish_t(obj, userdata, mid):
        obj.logger.info('Message '+ str(mid) + ' published.')
        return

    ## on_subscribe
    #
    #   This method is fired when we subscribe to a message.
    #
    #   @param userdata                     - the passed userdata
    #   @param mid                          - the message id
    #   @param granted_qos                  - the priority granted to the request
    #
    @staticmethod
    def on_subscribe_t(obj, userdata, mid, granted_qos):

        obj.logger.info('Subscribe with mid '+ str(mid) +' received.')
        return

    ## on_unsubscribe
    #
    #   This method is fired when we unsubscribe.
    #
    #   @param userdata                     - the passed userdata
    #   @param mid                          - the message id
    #
    @staticmethod
    def on_unsubscribe_t(obj, userdata, mid):

        obj.logger.info('Unsubscribe with mid ' + str(mid) + ' received.')
        return