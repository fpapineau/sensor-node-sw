__author__ = 'francispapineau'

#####################################################################
######################## IMPORTS ####################################
#####################################################################

from abc import abstractmethod
from Plugin_Engine.PluginTypes.Base_Plugin import base_plugin

#####################################################################
######################## CONSTANTS ##################################
#####################################################################

# N/A

#####################################################################
######################## CODE #######################################
#####################################################################

## client_plugin
#
#   This is a generic server plugin class that is used for any
#   app that needs to transmit data to another process
#
class server_plugin(base_plugin):

    # Auto start flag
    start                               = True

    # The commands available
    required_plugin_commands            = {
                                           "PROCESS"    : ['start',     'stop'],
                                           "CONNECTION" : [ 'connect',  'disconnect']
                                           }

    ## __init__
    #
    # The default Constructor
    #
    def __init__(self):
        base_plugin.__init__(self, self.start)
        self.valid_commands.append(self.required_plugin_commands)
        return

    ## disconnect
    #
    #   disconnects the client and shuts it down
    #
    @abstractmethod
    def disconnect(self):
        raise NotImplemented

    ## connect
    #
    #   Connects the client and starts it up
    #
    @abstractmethod
    def connect(self):
        raise NotImplemented
