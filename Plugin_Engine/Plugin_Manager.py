#####################################################################
######################## IMPORTS ####################################
#####################################################################

import ConfigParser
import logging

from Plugin_Engine.Print import print_commands
from Plugin_Engine.Shell_Process import shell
from yapsy.PluginManager import PluginManagerSingleton
from yapsy.VersionedPluginManager import VersionedPluginManager
from yapsy.ConfigurablePluginManager import ConfigurablePluginManager

from Plugin_Engine.System_Core import system_context

from Plugin_Engine.Plugins.Plugin_Map import PLUGIN_MAP

__author__ = 'francispapineau'

#####################################################################
######################## CONSTANTS ##################################
#####################################################################

# n/a

#####################################################################
######################## CODE #######################################
#####################################################################


## plugin_manager
#
#   This manages the plugin engine
#
class plugin_manager(object):

    # The manager object
    manager         = None

    # Console
    console         = None

    # Create a logger object
    logger_object   = logging.getLogger("plugin_manager")

    # commands
    valid_commands  = {}

    # plugins
    plugins         = {}

    # Config parser
    config          = None
    write_config    = None

    # Constext
    system          = system_context()
    
    # Plugin locations
    PLUGIN_BASE_LOCATION = ['./Plugins']

    # json handles
    json_engine     = None
    json_updated    = None

    ## __init__
    #
    #   The class constructor.
    #
    def __init__(self):

        # Get a safeconfig parser
        self.config = ConfigParser.ConfigParser()

        PluginManagerSingleton.setBehaviour([
            VersionedPluginManager,
            ConfigurablePluginManager
        ])

        # Start the system context
        self.system.setup()

        # Set the plugin manager and load the plugins
        self.manager = PluginManagerSingleton.get()
        self.manager.app = self
        self.manager.setConfigParser(self.config, self.write_config)
        self.manager.setPluginInfoExtension("plugin")

        # Set the places where the plugins are
        self.manager.setPluginPlaces(self.PLUGIN_BASE_LOCATION)
        self.manager.collectPlugins()
        self.logger_object.info("Loading the plugins: \n")
        
        # Print the loaded plugin signatures
        for plugin in self.manager.getAllPlugins():

            # Print the plugin signatures
            self.logger_object.info(("Loading: %s "
                                    %plugin.name).ljust(75, '.') +
                                    " [ Ok ]\n")

            self.logger_object.info(" ***************************************")
            self.logger_object.info("  - Name          : %s ", plugin.name)
            self.logger_object.info("  - Version       : %s ", plugin.version)
            self.logger_object.info("  - Author        : %s ", plugin.author)
            self.logger_object.info("  - Signature     : %s ", plugin.plugin_object.get_signature())
            self.logger_object.info(" ***************************************")
            self.logger_object.info("  - Valid Commands:")
            print_commands(plugin.plugin_object.get_valid_commands(), self.logger_object)
            
            # convert class
            plugin.plugin_object.__class__ = PLUGIN_MAP[plugin.name]
            
            # Store the handle
            self.plugins[plugin.name] = plugin.plugin_object

            # We get the valid commands from the automatic loaded plugins
            temp = plugin.plugin_object.get_valid_commands()
            if temp is not []:
                self.valid_commands[plugin.name] = temp

            # Activate
            self.manager.activatePluginByName(plugin.__class__.__name__)
            
            # We setup the plugin
            plugin.plugin_object.setup()
            
            # We check to see if it has an autostart function
            if plugin.plugin_object.auto_start:
                plugin.plugin_object.run()
        
        # Start the console
        self.console = shell(self.manager, self.valid_commands, self.plugins)
        self.console.run()
        return

if __name__ == "__main__":

    import logging
    from HomeMon_Logger_Handler import logger
    _logger = logger()
    log = logging.getLogger('Main')
    plugin_manager()

